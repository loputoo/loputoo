@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Muuda kasutajat</div>

                    <div class="panel-body">
                        @if ($errors->count() > 0)
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form action="{{ route('users.update', $user->id) }}" method="post">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            @if(strtoupper($user->name)=='TEST' || strtoupper($user->name)=='TESTS' || strtoupper($user->name)=='ADMIN')
                            Nimi:
                            <br />
                            <input class="form-control" type="text" style="background-color: lightgray;" name="name" value="{{ $user->name }}" disabled/>
                            <br />
                            Email:
                            <br />
                            <input class="form-control" type="text" name="email" value="{{ $user->email }}" />
                            <br />
                            @else
                                Nimi:
                                <br />
                                <input class="form-control" type="text" name="name" value="{{ $user->name }}"/>
                                <br />
                                Email:
                                <br />
                                <input class="form-control" type="text" name="email" value="{{ $user->email }}" />
                                <br />
                            @endif
                            <input class="form-control" type="submit" value="Submit" class="btn btn-default" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection