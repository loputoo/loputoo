@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (session('message'))
                    <div class="alert alert-info">{{ session('message') }}</div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Kasutajad</div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nimi</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users as $user)
                                @if(strtoupper($user->name)=='TEST' || strtoupper($user->name)=='ADMIN' )
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td><!-- Edit and Delete Buttons -->
                                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-default">Muuda</a>
                                    </td>
                                </tr>
                                @else
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td><!-- Edit and Delete Buttons -->
                                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-default">Muuda</a>
                                            <form action="{{ route('users.destroy', $user->id) }}" method="POST"
                                                  style="display: inline"
                                                  onsubmit="return confirm('Kas sa oled kindel?');">
                                                <input type="hidden" name="_method" value="DELETE">
                                                {{ csrf_field() }}
                                                <button class="btn btn-danger">Kustuta</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                            @empty
                                <tr>
                                    <td colspan="3">Ei leidnud ühtegi kirjet</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
