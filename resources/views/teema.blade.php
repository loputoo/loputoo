<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>


    <title>{{$data['teemaNimi']}} - {{config('app.name')}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animations.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
</head>
<body id="page-fade" style="display: none;">
<div class="title"><?php echo $data['teemaNimi']; ?></div>
<div class="page-container">
    <div id="page-menu-list" class="page-menu">
        <ul>
            <a href="{{url("/")}}"><li class="back-button">Tagasi</li></a>
            <a href="javascript:changePage(1)"><li id="tab-1" class="active">Pealeht</li></a>
            <a href="javascript:changePage(2)"><li id="tab-2">Valemid</li></a>
            <a href="javascript:changePage(3)"><li id="tab-3">Kalkulaator</li></a>
            <a href="javascript:changePage(4)"><li id="tab-4">Testid</li></a>
        </ul>

        <select name="dropdown-menu">
            <option value="default">Menüü</option>
            <option value="{{url("/")}}" class="back-button">Tagasi</option>
            <option value="javascript:changePage(1)" class="bac">Pealeht</option>
            <option value="javascript:changePage(2)">Valemid</option>
            <option value="javascript:changePage(3)">Kalkulaator</option>
            <option value="javascript:changePage(4)">Testid</option>
        </select>
    </div>
    <div class="page-content">
        @include("sections.sections".$data['id'])
    </div>
</div>

<script>
    $('#page-fade').fadeIn(1200);

    var pageCount = 5;
    initPage();
    $('#section-5').hide();

    function initPage() {
        for (var i = 1; i < pageCount; i++) {
            $('#section-' + (i).toString()).hide();
        }

        $('#section-1').show();
        $('#section-5').hide();

        switchCalc(1);
    }

    function changePage(page) {
        for (var i = 1; i < pageCount; i++) {
            $('#tab-'+ (i).toString()).removeClass('active');
        }

        for (var j = 1; j < pageCount; j++) {
            $('#section-' + (j).toString()).hide();
        }

        $('#section-'+page.toString()).show();
        $('#tab-'+page.toString()).addClass('active');

        $('#section-5').hide();
    }

    function revealTestResult(bool) {
        changePage(4);

        if(bool) {
            $('#section-5').show();
            $('#section-4').hide();
        } else {
            $('#section-5').hide();
            $('#tests-form').html('');
            $('#tests-form').hide();
            $('#start-test-form').show();
            $('#section-4').show();
        }
    }

    function getTests(id) {
        $('#start-test-form').fadeOut(250);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            method: 'POST',
            url: '{{url('teema/'.$data['id'].'/getTests')}}',
            data: $('#tests-form').serialize(),
            success: function(response){
                var qid = 0;
                var htmlOutput = '';
                response['tests'].forEach(function(element) {
                    var answArr = [element['choiceOne'], element['choiceTwo'], element['choiceThree'], element['answer']];
                    answArr = shuffle(answArr);

                    htmlOutput += '<h3>' + element['question'] + '</h3>' +
                        '<input class="test-radio" type="radio" name="question'+ qid +'" value="'+ element['id'] +':'+ answArr[0] +'">'+answArr[0]+'<br />' +
                        '<input class="test-radio" type="radio" name="question'+ qid +'" value="'+ element['id'] +':'+ answArr[1] +'">'+answArr[1]+'<br />' +
                        '<input class="test-radio" type="radio" name="question'+ qid +'" value="'+ element['id'] +':'+ answArr[2] +'">'+answArr[2]+'<br />' +
                        '<input class="test-radio" type="radio" name="question'+ qid +'" value="'+ element['id'] +':'+ answArr[3] +'">'+answArr[3]+'<br />';

                    qid++;
                });

                htmlOutput += '<input type="submit" value="Lõpeta" class="btn-success test-submit">';
                $('#tests-form').append(htmlOutput);
                setTimeout(function () {
                    $('#tests-form').fadeIn(250);
                }, 300)
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }

    function shuffle(a) {
        var j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
        return a;
    }

    function sendTestData() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            method: 'POST',
            url: '{{url('teema/'.$data['id'].'/getResults')}}',
            data: $('#tests-form').serialize(),
            success: function(response){
                if(typeof response['error'] !== 'undefined') {
                    $('#calcErrorOutput').html(response['error']);
                    showErrorDial();
                } else {
                    revealTestResult(true);
                    $('#tests-result').html(response['score'] + response['result']);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }

    function sendCalculatorData(url, index) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        var formString = '#calculator-data';

        if(index !== '') {
            formString+=index;
        }

        $.ajax({
            method: 'POST',
            url: url,
            data: $(formString).serialize(),
            success: function(response){
                if(typeof response['calcData']['error'] !== 'undefined') {
                    $('#calcErrorOutput').html(response['calcData']['error']);
                    showErrorDial();
                } else {
                    if (response['calcData']['a'] !== '') {
                        $('#co_a'+index).html("a: "+response['calcData']['a']);
                    }

                    if (response['calcData']['b'] !== '') {
                        $('#co_b'+index).html("b: "+response['calcData']['b']);
                    }

                    if (response['calcData']['c'] !== '') {
                        $('#co_c'+index).html("c: "+response['calcData']['c']);
                    }

                    if (response['calcData']['d'] !== '') {
                        $('#co_d'+index).html("d: "+response['calcData']['d']);
                    }

                    if (response['calcData']['d1'] !== '') {
                        $('#co_d1'+index).html("d<sub>1</sub>: "+response['calcData']['d1']);
                    }

                    if (response['calcData']['d2'] !== '') {
                        $('#co_d2'+index).html("d<sub>2</sub>: "+response['calcData']['d2']);
                    }

                    if (response['calcData']['h'] !== '') {
                        $('#co_h'+index).html("h: "+response['calcData']['h']);
                    }

                    if (response['calcData']['k'] !== '') {
                        $('#co_k'+index).html("k: "+response['calcData']['k']);
                    }

                    if (response['calcData']['p'] !== '') {
                        $('#co_p'+index).html("P: "+response['calcData']['p']);
                    }

                    if (response['calcData']['s'] !== '') {
                        $('#co_s'+index).html("S: "+response['calcData']['s']+"<sup>2</sup>");
                    }

                    if (response['calcData']['sp'] !== '') {
                        $('#co_sp'+index).html("S<sub>p</sub>: "+response['calcData']['sp']+"<sup>2</sup>");
                    }

                    if (response['calcData']['sk'] !== '') {
                        $('#co_sk'+index).html("S<sub>k</sub>: "+response['calcData']['sk']+"<sup>2</sup>");
                    }

                    if (response['calcData']['st'] !== '') {
                        $('#co_st'+index).html("S<sub>t</sub>: "+response['calcData']['st']+"<sup>2</sup>");
                    }

                    if (response['calcData']['v'] !== '') {
                        $('#co_v'+index).html("V: "+response['calcData']['v']+"<sup>3</sup>");
                    }

                    if (response['calcData']['alp'] !== '') {
                        $('#co_alp'+index).html("&ang;A: "+response['calcData']['alp']+"&deg;");
                    }

                    if (response['calcData']['bet'] !== '') {
                        $('#co_bet'+index).html("&ang;B: "+response['calcData']['bet']+"&deg;");
                    }

                    if (response['calcData']['gam'] !== '') {
                        $('#co_gam'+index).html("&ang;C: "+response['calcData']['gam']+"&deg;");
                    }

                    if (response['calcData']['del'] !== '') {
                        $('#co_del'+index).html("&ang;D: "+response['calcData']['del']+"&deg;");
                    }

                    $('#calc-result'+index).html(response['calcData']['result']);

                    calculated(index);
                }
            }
        });
    }

    function switchCalc(id) {

        for (var j = 1; j < 15; j++) {
            if($('#calc-' + (j)).length) {
                $('#calc-' + (j)).hide();
            } else {
                break;
            }
        }

        $('#calc-'+id).fadeIn(300);
    }

    function typeSelected(obj, val) {
        switch(val) {
            case "Ruut": case "Kuup":
                switchCalc(1);
                break;
            case "Ristkylik": case "Risttahukas":
                switchCalc(2);
                break;
            case "Romb": case "Pystprisma":
                switchCalc(3);
                break;
            case "Roopkylik": case "Rooptahukas":
                switchCalc(4);
                break;
            case "Trapets": case "KorraparanePyramiid":
                switchCalc(5);
                break;
            case "Silinder":
                switchCalc(6);
                break;
            case "Koonus":
                switchCalc(7);
                break;
            case "Kera":
                switchCalc(8);
                break;
            default:
                switchCalc(1);
                break;
        }

        $('#'+obj).val(val);
    }

    function showErrorDial() {
        $('#background-fade').fadeIn(250);
        $('#calc_popup').fadeIn(400);
    }

    function closeErrorDial() {
        $('#background-fade').fadeOut(400);
        $('#calc_popup').fadeOut(250);
    }

    function calculated(index) {
        if($('#disableOnCalc'+index).is(':visible')) {
            $('#disableOnCalc'+index).fadeOut(150);
        }
        if($('#enableOnCalc'+index).is(':hidden')) {
            $('#enableOnCalc'+index).fadeIn(300);
        }
    }

    $("#page-menu-list select option[value='default']").hide();

    $("#page-menu-list select").change(function() {
        window.location = $(this).find("option:selected").val();
    });
</script>
</body>
</html>