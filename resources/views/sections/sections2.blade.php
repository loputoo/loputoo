<div class="page-section" id="section-1">
    <div class="row row-shadow">
        <div class="section-header col-12">
            Ruut
        </div>
        <div class="col-9 description-row">
            Ruuduks nimetatakse võrdsete külgedega ristkülikut.<br>
            Ruudu kõik küljed on võrdse pikkusega.<br>
            Ruudu kõik nurgad on täisnurgad ehk 90kraadi.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/87.png")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Ristkülik
        </div>
        <div class="col-9 description-row">
            Ristkülik on nelinurk, mille kõik nurgad on täisnurgad.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/90.png")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Romb
        </div>
        <div class="col-9 description-row">
            Rombiks nimetatakse rööpkülikut, mille kõik küljed on võrdsed.<br>
            Kuna romb on rööpkülik, siis kõik rööpküliku omadused on ka rombi omadused.<br>
            Rombi omadused:
            <li>Rombi kõik küljed on võrdsed</li>
            <li>Romb on sümmeetriline diagonaalide suhtes</li>
            <li>Rombi diagonaalid on teineteisega risti ja poolitavad rombi nurgad</li>
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/102.png")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Rööpkülik
        </div>
        <div class="col-9 description-row">
            Rööpkülikuks nimetatakse nelinurka, mille vastasküljed on paralleelsed.<br>
            Rööpküliku vastasküljed on võrdsed.<br>
            Rööpküliku diagonaalid poolitavad üksteist.<br>
            Rööpüliku diagonaalid jaotavad rööpküliku kaheks võrdseks kolmnurgaks.<br>
            Rööpküliku vastasnurgad on võrdsed.<br>
            Rööküliku lähisnurkade summa on 180kraadi.<br>
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/96.png")}}">
        </div>
    </div>

    <div class="row">
        <div class="section-header col-12">
            Trapets
        </div>
        <div class="col-9 description-row">
            Trapetsiks nimetatakse nelinurka, mille kaks külge on paralleelsed ja kaks mitteparalleelsed.<br>
            Trapetsi paralleelseid külgi nimetatakse alusteks ja mitteparalleelseid külgi trapetsi haaradeks.<br>
            Trapetsi omadused:
            <li>Alused on paralleelsed</li>
            <li>Trapetsi haara lähisnurkade summa on 180kraadi</li>
            <br>
            Trapetsit, mille haarad on võrdsed, nimetatakse võrdhaarseks trapetsiks.<br>
            Võrdhaarse trapetsi omadused:
            <li>Haarad on võrdsed</li>
            <li>Aluse lähisnurgad on võrdsed</li>
            <li>DIagonaalid on võrdsed</li>
            <br>
            Trapetsit, mille üks haar on risti alustega, nimetatakse täisnurkseks trapetsiks.<br>
            Täisnurkse trapetsi omadused:
            <li>Trapetsi lühem haar on risti alustega ja selle haara lähisnurgad on 90kraadi</li>
            <br>
            Lõiku, mis ühendab trapetsi haarade keskpunkte, nimetatakse trapetsi kesklõiguks.<br>
            Trapetsi kesklõik on alustega paralleelne ja võrdub aluste poolsummaga.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/106.png")}}">
        </div>
    </div>
</div>
<div class="page-section" id="section-2">
    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Ruut
        </div>
        <div class="section-formula-header col-12">
            Ümbermõõt
        </div>
        <div class="col-7 description-row">
            Ruudu ümbermõõt saadakse kätte, liites omavahel kõik küljed.
        </div>
        <div class="col-3 text-center formula">
            P = 4a
        </div>
        <div class="section-formula-header col-12">
            Pindala
        </div>
        <div class="col-7 description-row">
            Ruudu pindala saadakse kätte, leides ruudu külje ruut.
        </div>
        <div class="col-3 text-center formula">
            S = a<sup>2</sup>
        </div>
        <div class="section-formula-header col-12">
            Diagonaal
        </div>
        <div class="col-7 description-row">
            Ruudu diagonaal saadakse kätte pythagorase teoreemiga.
        </div>
        <div class="col-3 text-center formula">
            d = &Sqrt;(a<sup>2</sup> + b<sup>2</sup>)
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Ristkülik
        </div>
        <div class="section-formula-header col-12">
            Ümbermõõt
        </div>
        <div class="col-7 description-row">
            Ristküliku ümbermõõt saadakse kätte, liites omavahel kõik küljed.
        </div>
        <div class="col-3 text-center formula">
            P = 2(a + b)
        </div>
        <div class="section-formula-header col-12">
            Pindala
        </div>
        <div class="col-7 description-row">
            Ristküliku pindala saadakse kätte, korrutades ristküliku kaks erinevat külge.
        </div>
        <div class="col-3 text-center formula">
            S = ab
        </div>
        <div class="section-formula-header col-12">
            Diagonaal
        </div>
        <div class="col-7 description-row">
            Ristküliku diagonaal saadakse kätte pythagorase teoreemiga.
        </div>
        <div class="col-3 text-center formula">
            d = &Sqrt;(a<sup>2</sup> + b<sup>2</sup>)
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Romb
        </div>
        <div class="section-formula-header col-12">
            Ümbermõõt
        </div>
        <div class="col-7 description-row">
            Rombi ümbermõõt saadakse kätte, liites omavahel kõik küljed.
        </div>
        <div class="col-3 text-center formula">
            P = 4a
        </div>
        <div class="section-formula-header col-12">
            Pindala
        </div>
        <div class="col-7 description-row">
            Rombi pindala saadakse kätte, korrutades rombi külg ja kõrgus.
        </div>
        <div class="col-3 text-center formula">
            S = ah<br> S = (d<sub>1</sub> * d<sub>2</sub>) / 2
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Rööpkülik
        </div>
        <div class="section-formula-header col-12">
            Ümbermõõt
        </div>
        <div class="col-7 description-row">
            Rööpküliku ümbermõõt saadakse kätte, liites omavahel kõik küljed.
        </div>
        <div class="col-3 text-center formula">
            P = 4a
        </div>
        <div class="section-formula-header col-12">
            Pindala
        </div>
        <div class="col-7 description-row">
            Rööpküliku pindala saadakse kätte, korrutades rööpküliku alus ja kõrgus või kaks erinevat külge.
        </div>
        <div class="col-3 text-center formula">
            S = ah<br> S = ab
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Trapets
        </div>
        <div class="section-formula-header col-12">
            Ümbermõõt
        </div>
        <div class="col-7 description-row">
            Trapetsi ümbermõõt saadakse kätte, liites omavahel kõik küljed.
        </div>
        <div class="col-3 text-center formula">
            P = a + b + c + d
        </div>
        <div class="section-formula-header col-12">
            Pindala
        </div>
        <div class="col-7 description-row">
            Trapetsi pindala saadakse kätte, korrutades trapetsi kaks külge jagades see kahega ning korrutades kõrgusega.<br>
            Pindala on võimalik leida ka korrutades kesklõiku kõrgusega.
        </div>
        <div class="col-3 text-center formula">
            S = (a + b) / 2 * h<br>
            S = k * h
        </div>
        <div class="section-formula-header col-12">
            Kesklõik
        </div>
        <div class="col-7 description-row">
            Trapetsi kesklõik saadakse kätte, leides aluste poolsumma.
        </div>
        <div class="col-3 text-center formula">
            k = (a + b) / 2
        </div>
    </div>
</div>
<div class="page-section" id="section-3">
    Vali tüüp:
    <select name="nelinurgad" onChange="javascript:typeSelected('nelinurgad', this.value)">
        <option value="Ruut">Ruut</option>
        <option value="Ristkylik">Ristkülik</option>
        <option value="Romb">Romb</option>
        <option value="Roopkylik">Rööpkülik</option>
        <option value="Trapets">Trapets</option>
    </select>

    <div id="calc-1" class="row col-lg-10 offset-lg-1">
        @include("calculators.squareCalc")
    </div>

    <div id="calc-2" class="row col-lg-10 offset-lg-1">
        @include("calculators.rectangleCalc")
    </div>

    <div id="calc-3" class="row col-lg-10 offset-lg-1">
        @include("calculators.rhombusCalc")
    </div>

    <div id="calc-4" class="row col-lg-10 offset-lg-1">
        @include("calculators.parallelogramCalc")
    </div>

    <div id="calc-5" class="row col-lg-10 offset-lg-1">
        @include("calculators.trapesiumCalc")
    </div>
</div>
<div class="page-section" id="section-4">
    @include("layouts.testLayout")
</div>
<div class="page-section" id="section-5">
    @include("layouts.testResult")
</div>
@include("layouts.calcPopup")