<div class="page-section" id="section-1">
    <div class="row row-shadow">
        <div class="section-header col-12">
            Kuup
        </div>
        <div class="col-9 description-row">
            Kuup on risttahukas, mille kõik mõõtmed on võrdsed.<br>
            Kuup on rummiline kujund ehk keha, mille tahkudeks on kuus ruutu.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/181.png")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Risttahukas
        </div>
        <div class="col-9 description-row">
            Risttahukas on ruumiline kujund ehk keha.<br>
            Risttahuka tahkudeks on ristkülikud.<br>
            Risttahuka vastastahud on võrdsed.<br>

            Tavapärased tähistused:
            <li>a - pikkus</li>
            <li>b - laius</li>
            <li>c - kõrgus</li>
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/180.png")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Prisma
        </div>
        <div class="col-8 description-row">
            Prisma on ruumiline kujund ehk keha, millel on kaks põhitahkus, mis on omavahel võrdsed ja asuvad paralleelsetel tasanditel. Põhitahke ühendavad külgtahud.<br>
            Püstprisma külgtahud on ristkülikud. Külgtahkude arv oleneb sellest, kui palju külgi on põhitahuks oleval hulknurgal.<br>
            Kui püstprisma põhjaks on kolmnurk, siis nimetatakse prismat kolmnurkseks püstprismaks, kui põhjaks on nelinurk, siis nelinurkseks püstprismaks jne. Ka risttahukad (sh kuubid) on nelinurksed püstprismad.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/182.png")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Rööptahukas
        </div>
        <div class="col-9 description-row">
            Rööptahukas on prisma, mille põhjad on rööpkülikud.<br>
            Rööptahuka kõik tahud on rööpkülikud, mis on samasugused oma vastastahkudega.<br>
            Rööptahuka kõrguseks nimetatakse selle põhjade vahelist kaugust.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/183.png")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Korrapärased püramiidid
        </div>
        <div class="col-9 description-row">
            Püramiidiks nimetatakse hulktahukat, mille üks tahk on hulknurk ja kõik ülejäänud on ühise tipuga kolmnurgad.<br>
            Hulknurka nimetatakse püramiidi põhjaks ja ühise tipuga kolmnurki külgtahkudeks. Püramiidi põhja külgi nimetatakse püramiidi põhiservadeks ja külgtahkude ühiseid servi külgservadeks.<br>
            Külgservade ühine punkt on püramiidi tipp. Tipu kaugus põhjas on püramiidi kõrgus h. Püramiidi külgtahu kõrgust nimetatakse apoteemiks m.<br>
            Püramiidi nimetatakse korrapäraseks siis, kui selel  põhjaks on korrapärane hulknurk ja püramiidi kõrguse aluspunkt asub põhja keskpunktis.<br>
            Korrapärase püramiidi kõik külgtahud on võrsed võrdhaarsed kolmnurgad.<br>
            Põhiservade arvu järgi jagatakse püramiidid kolmnurkseteks, nelinurkseteks, viisnurkseteks jne, ehk üldiselt n-nurkseteks.<br>
            Püramiidi kõrgus jaotab kolmnurkse põhja kõrguse osadeks, kus 2/3 põhja kõrgusest jääb kolmnurga tipu poolsesse osasse ja 1/3 kolmnurga aluse poolsesse osasse.<br>
            Korrapärane tetraeeder ehk korrapärane nelitahukas ehk regulaarne nelitahukas on korrapärane hulktahukas, millel on neli võrdkülgset kolmnurga kujulist tahku ja mille igast tipust lähtub kolm serva.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/184.png")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Silinder
        </div>
        <div class="col-9 description-row">
            Silinder on keha, mille moodustab ümber oma ühe külje pöörlev ristkülik.<br>
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/193.png")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Koonus
        </div>
        <div class="col-9 description-row">
            Koonus on keha, mille moodustab ühe oma kaateti ümber pöörlev täisnurkne kolmnurk.<br>
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/197.png")}}">
        </div>
    </div>

    <div class="row">
        <div class="section-header col-12">
            Kera
        </div>
        <div class="col-9 description-row">
            Kera on pöörkeha. Keraks nimetatakse keha, mis tekib ringi pöörlemisel ümber oma diameetri.<br>
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{url("https://www.taskutark.ee/m/wp-content/uploads/sites/2/2015/02/201.png")}}">
        </div>
    </div>

</div>
<div class="page-section" id="section-2">
    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Kuup
        </div>
        <div class="section-formula-header col-12">
            Täispindala
        </div>
        <div class="col-7 description-row">
            Kuubi täispindala saadakse kätte, liites omavahel kõikide küljgede pindalad.
        </div>
        <div class="col-3 text-center formula">
            S<sub>t</sub> = 6a<sup>2</sup>
        </div>
        <div class="section-formula-header col-12">
            Ruumala
        </div>
        <div class="col-7 description-row">
            Kuubi ruumala saadakse kätte, arvutades kuubi külje kuup.
        </div>
        <div class="col-3 text-center formula">
            V = a<sup>3</sup>
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Risttahukas
        </div>
        <div class="section-formula-header col-12">
            Täispindala
        </div>
        <div class="col-7 description-row">
            Risttahuka täispindala saadakse kätte, liites omavahel kõikide küljgede pindalad.
        </div>
        <div class="col-3 text-center formula">
            S<sub>t</sub> = 2(ab + ac + bc)
        </div>
        <div class="section-formula-header col-12">
            Ruumala
        </div>
        <div class="col-7 description-row">
            Risttahuka ruumala saadakse kätte, korrutades risttahuka pikkus, laius ja kõrgus.
        </div>
        <div class="col-3 text-center formula">
            V = abc
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Risttahukas
        </div>
        <div class="section-formula-header col-12">
            Täispindala
        </div>
        <div class="col-7 description-row">
            Risttahuka täispindala saadakse kätte, liites omavahel kõikide küljgede pindalad.
        </div>
        <div class="col-3 text-center formula">
            S<sub>t</sub> = 2(ab + ac + bc)
        </div>
        <div class="section-formula-header col-12">
            Ruumala
        </div>
        <div class="col-7 description-row">
            Risttahuka ruumala saadakse kätte, korrutades risttahuka pikkus, laius ja kõrgus.
        </div>
        <div class="col-3 text-center formula">
            V = abc
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Prisma
        </div>
        <div class="section-formula-header col-12">
            Põhjapindala
        </div>
        <div class="col-7 description-row">
            Kujundi pindala, mis parajasti prisma põhjaks on.
        </div>
        <div class="section-formula-header col-12">
            Külgpindala
        </div>
        <div class="col-7 description-row">
            Prisma külgpindala saadakse kätte, korrutades omavahel põhja ümbermõõt ja kõrgus.
        </div>
        <div class="col-3 text-center formula">
            S<sub>k</sub> = Ph
        </div>
        <div class="section-formula-header col-12">
            Täispindala
        </div>
        <div class="col-7 description-row">
            Prisma täispindala saadakse kätte, liites omavahel küljepindala ja kaks põhjapindala.
        </div>
        <div class="col-3 text-center formula">
            S<sub>t</sub> = Sk + 2 * Sp
        </div>
        <div class="section-formula-header col-12">
            Ruumala
        </div>
        <div class="col-7 description-row">
            Prisma ruumala saadakse kätte, korrutades prisma põhjapindala kõrgusega.
        </div>
        <div class="col-3 text-center formula">
            V = S<sub>p</sub> * h
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Rööptahukas
        </div>
        <div class="section-formula-header col-12">
            Põhjapindala
        </div>
        <div class="col-7 description-row">
            Rööptahuka põhjaks on rööpkülik - järelikult rööptahuka põhjapindala saab arvutada rööpküliku pindala valemit kasutades.
        </div>
        <div class="section-formula-header col-12">
            Külgpindala
        </div>
        <div class="col-7 description-row">
            Rööptahuka külgpindala saadakse kätte, korrutades omavahel põhja ümbermõõt ja kõrgus.
        </div>
        <div class="col-3 text-center formula">
            S<sub>k</sub> = PH
        </div>
        <div class="section-formula-header col-12">
            Täispindala
        </div>
        <div class="col-7 description-row">
            Rööptahuka täispindala saadakse kätte, liites omavahel küljepindala ja kaks põhjapindala.
        </div>
        <div class="col-3 text-center formula">
            S<sub>t</sub> = Sk + 2Sp
        </div>
        <div class="section-formula-header col-12">
            Ruumala
        </div>
        <div class="col-7 description-row">
            Rööptahuka ruumala saadakse kätte, korrutades prisma põhjapindala kõrgusega.
        </div>
        <div class="col-3 text-center formula">
            V = S<sub>p</sub>H
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Korrapärased püramiidid
        </div>
        <div class="section-formula-header col-12">
            Põhjapindala
        </div>
        <div class="col-7 description-row">
            põhjaks oleva hulknurga pindala.
        </div>
        <div class="section-formula-header col-12">
            Külgpindala
        </div>
        <div class="col-7 description-row">
            Korrapärase püramiidi külgpindala saadakse kätte, korrutades omavahel põhja ümbermõõdu ja apoteemi ning see jagada kahega.
        </div>
        <div class="col-3 text-center formula">
            S<sub>k</sub> =  P * m / 2
        </div>
        <div class="section-formula-header col-12">
            Täispindala
        </div>
        <div class="col-7 description-row">
            Korrapärase püramiidi täispindala saadakse kätte, liites omavahel küljepindala ja põhjapindala.
        </div>
        <div class="col-3 text-center formula">
            S<sub>t</sub> = S<sub>p</sub> + S<sub>k</sub>
        </div>
        <div class="section-formula-header col-12">
            Ruumala
        </div>
        <div class="col-7 description-row">
            Korrapärase püramiidi ruumala saadakse kätte, korrutades põhjapindala kõrgusega ja jagade see kolmega.
        </div>
        <div class="col-3 text-center formula">
            V = S<sub>p</sub> * H / 3
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Silinder
        </div>
        <div class="section-formula-header col-12">
            Põhjapindala
        </div>
        <div class="col-7 description-row">
            Silindri põhjadeks on ringid, seega põhja pindalaks on ringi pindala.
        </div>
        <div class="section-formula-header col-12">
            Külgpindala
        </div>
        <div class="col-7 description-row">
            Silindri külgpindala saadakse kätte, korrutades omavahel põhja ümbermõõdu ja kõrguse.
        </div>
        <div class="col-3 text-center formula">
            S<sub>k</sub> =  P * h
        </div>
        <div class="section-formula-header col-12">
            Täispindala
        </div>
        <div class="col-7 description-row">
            Silindri täispindala saadakse kätte, liites omavahel küljepindala ja kaks põhjapindala.
        </div>
        <div class="col-3 text-center formula">
            S<sub>t</sub> = 2S<sub>p</sub> + S<sub>k</sub>
        </div>
        <div class="section-formula-header col-12">
            Ruumala
        </div>
        <div class="col-7 description-row">
            Silindri ruumala saadakse kätte, korrutades Silindri põhjapindala kõrgusega.
        </div>
        <div class="col-3 text-center formula">
            V = S<sub>p</sub> * h
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Koonus
        </div>
        <div class="section-formula-header col-12">
            Põhjapindala / Põhja ümbermõõt
        </div>
        <div class="col-7 description-row">
            Koonuse põhjaks on ring - järelikult põhja ümbermõõt ning pindala on ringi ümbermõõt ja pindala
        </div>
        <div class="section-formula-header col-12">
            Külgpindala
        </div>
        <div class="col-7 description-row">
            Koonuse külgpindala saadakse kätte, korrutades omavahel põhja ümbermõõdu ja apoteem ning see jagada kahega.
        </div>
        <div class="col-3 text-center formula">
            S<sub>k</sub> =  P * m / 2
        </div>
        <div class="section-formula-header col-12">
            Täispindala
        </div>
        <div class="col-7 description-row">
            Koonuse täispindala saadakse kätte, liites omavahel küljepindala ja kaks põhjapindala.
        </div>
        <div class="col-3 text-center formula">
            S<sub>t</sub> = 2S<sub>p</sub> + S<sub>k</sub>
        </div>
        <div class="section-formula-header col-12">
            Ruumala
        </div>
        <div class="col-7 description-row">
            Koonuse ruumala saadakse kätte, korrutades põhjapindala kõrgusega ja jagade see kolmega.
        </div>
        <div class="col-3 text-center formula">
            V = S<sub>p</sub> * h / 3
        </div>
    </div>

    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Kera
        </div>
        <div class="section-formula-header col-12">
            Pindala
        </div>
        <div class="col-7 description-row">
            Kera külgpindala saadakse kätte, korrutades omavahel &pi; ja raadiuse ruut ning see korrutada neljaga.
        </div>
        <div class="col-3 text-center formula">
            S= &pi; * r<sup>2</sup> * 4
        </div>
        <div class="section-formula-header col-12">
            Ruumala
        </div>
        <div class="col-7 description-row">
            Kera ruumala saadakse kätte, korrutades &pi; raadiuse kuubiga ning see jagada kolmega ja korrutada neljaga.
        </div>
        <div class="col-3 text-center formula">
            V= &pi; * r<sup>3</sup> / 3 * 4
        </div>
    </div>
</div>
<div class="page-section" id="section-3">
    Vali tüüp:
    <select name="ruumilised" onChange="javascript:typeSelected('ruumilised', this.value)">
        <option value="Kuup">Kuup</option>
        <option value="Risttahukas">Risttahukas</option>
        <option value="Pystprisma">Püstprisma</option>
        <option value="Rooptahukas">Rööptahukas</option>
        <option value="KorraparanePyramiid">Korrapärane püramiid</option>
        <option value="Silinder">Silinder</option>
        <option value="Koonus">Koonus</option>
        <option value="Kera">Kera</option>
    </select>

    <div id="calc-1" class="row col-lg-10 offset-lg-1">
        @include("calculators.cubeCalc")
    </div>

    <div id="calc-2" class="row col-lg-10 offset-lg-1">
        @include("calculators.rectangularCalc")
    </div>

    <div id="calc-3" class="row col-lg-10 offset-lg-1">
        @include("calculators.uprightPrismCalc")
    </div>

    <div id="calc-4" class="row col-lg-10 offset-lg-1">
        @include("calculators.paralleloPrismCalc")
    </div>

    <div id="calc-5" class="row col-lg-10 offset-lg-1">
        @include("calculators.trapesiumCalc")
    </div>

    <div id="calc-6" class="row col-lg-10 offset-lg-1">
        @include("calculators.trapesiumCalc")
    </div>

    <div id="calc-7" class="row col-lg-10 offset-lg-1">
        @include("calculators.trapesiumCalc")
    </div>

    <div id="calc-8" class="row col-lg-10 offset-lg-1">
        @include("calculators.trapesiumCalc")
    </div>
</div>
<div class="page-section" id="section-4">
    @include("layouts.testLayout")
</div>
<div class="page-section" id="section-5">
    @include("layouts.testResult")
</div>
@include("layouts.calcPopup")