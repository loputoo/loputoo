<div class="page-section" id="section-1">
    <div class="row row-shadow">
        <div class="section-header col-12">
            Täisnurkne kolmnurk
        </div>
        <div class="col-9 description-row">
            Kolmnurk, mille üks nurk on 90&deg;, nimetatakse täisnurkseks kolmnurgaks.<br>
            Täisnurga lähiskülgesid nimetatakse kaatetiteks ja selle vastaskülge hüpotenuusiks.<br>
            Täisnurkse kolmnurga hüpotenuusi tähistatakse enamasti tähega c ning kaateteid tähtedega a ja b.<br>
            Hüpotenuus on alati pikem kummastki kaatetist. Hüpotenuusi lähisnurgad on väiksemad täisnurgast ja nende summa võrdub täisnurgaga.<br>
            Täisnurkne kolmnurk võib olla erikülgne või võrdhaarne, kuid mitte võrdkülgne.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{asset("/images/kolmnurk/taisnurkne_kolmnurk.svg")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Nürinurkne kolmnurk
        </div>
        <div class="col-9 description-row">
            Kolmnurk, mille üks nurk on suurem kui 90&deg;, nimetatakse nürinurksesks kolmnurgaks.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{asset("/images/kolmnurk/nyrinurkne_kolmnurk.svg")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Teravnurkne kolmnurk
        </div>
        <div class="col-9 description-row">
            Kolmnurk, mille kõik nurgad on võiksemad kui 90&deg;, nimetatakse teravnurkseks kolmnurgaks.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{asset("/images/kolmnurk/teravnurkne_kolmnurk.svg")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Erikülgne kolmnurk
        </div>
        <div class="col-9 description-row">
            Erikülgne kolmnurk on kolmnurk, millel kõik küljed on erineva pikkusega.<br>
            Erikülgse kolmnurga kõik nurgad on samuti erineva suurusega.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{asset("/images/kolmnurk/erikylgne_kolmnurk.svg")}}">
        </div>
    </div>

    <div class="row row-shadow">
        <div class="section-header col-12">
            Võrdhaarne kolmnurk
        </div>
        <div class="col-9 description-row">
            Võrdhaarne kolmnurk on kolmnurk, mille kaks külge on võrdse pikkusega.<br>
            Võrdhaarse kolmnurga haaradeks nimetatakse kahte võrdset külge ja aluseks kolmandat külge.<br>
            Tipunurgaks nimetatakse selle kolmnurga kahe võrdse külje vahelist nurka.<br>
            Võrdhaarse kolmnurga kaht teist nurka nimetatakse alusnurkadeks.<br>
            Võrdhaarse kolmnurga alusnurgad on võrdsed.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{asset("/images/kolmnurk/vordhaarne_kolmnurk.svg")}}">
        </div>
    </div>

    <div class="row">
        <div class="section-header col-12">
            Võrdkülgne kolmnurk
        </div>
        <div class="col-9 description-row">
            Võrdkülgne kolmnurk on kolmnurk, mille kõik küljed on võrdse pikkusega.<br>
            Võrdkülgse kolmnurga kõik nurgad on samuti võrdse suurusega 60°.<br>
            Võrdkülgne kolmnurk on korrapärane hulknurk.
        </div>
        <div class="col-3">
            <img class="page-content-image" src="{{asset("/images/kolmnurk/vordkylgne_kolmnurk.svg")}}">
        </div>
    </div>
</div>
<div class="page-section" id="section-2">
    <div class="row" style=" padding-bottom: 8px; border: 1px solid rgba(0,0,0,0.1);">
        <div class="section-header col-12 row-shadow">
            Kolmnurk
        </div>
        <div class="section-formula-header col-12">
            Ümbermõõt
        </div>
        <div class="col-7 description-row">
            Kolmnurga ümbermõõt saadakse kätte, liites omavahel kõik küljed.
        </div>
        <div class="col-3 text-center formula">
            P = a + b + c
        </div>
        <div class="section-formula-header col-12">
            Pindala
        </div>
        <div class="col-7 description-row">
            Kolmnurga pindala saadakse kätte, korrutades aluse kõrgusega (täisnurkse kolmnurga puhul korrutatakse kaatetid omavahel) ja jagatakse kahega.
        </div>
        <div class="col-3 text-center formula">
            S = ah / 2<br>
            S = ab / 2
        </div>
        <div class="section-formula-header col-12">
            Nurk
        </div>
        <div class="col-7 description-row">
            Kuna kolmnurga sisenurgad võrduvad alati kokku 180&deg; siis on vaja teada vähemalt kahte nurka, et leida kolmas.
        </div>
        <div class="col-3 text-center formula">
            &ang;A = 180&deg; - &ang;B - &ang;C
        </div>
    </div>
</div>
<div class="page-section" id="section-3" style="padding: 10px">
    @include("calculators.triangleCalc")
</div>
<div class="page-section" id="section-4">
    @include("layouts.testLayout")
</div>
<div class="page-section" id="section-5">
    @include("layouts.testResult")
</div>
@include("layouts.calcPopup")