<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Põhikooli {{ config('app.name') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animations.css') }}" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .test-color {
            background-color: rgba(10, 10, 10, 0.24);
        }
    </style>
</head>
<body>
<div id="page-fade" class="flex-center full-height container" style="display: none;">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ url('login') }}">Login</a>
            @endauth
        </div>
    @endif
    <div class="content">
        <div class="title m-b-md puff-in-center">Põhikooli {{ config('app.name') }}</div>
        <div id="categories" class="links">
            @foreach ($data['themes'] as $row)
                <a id="theme-box-{{$row->id}}" class="selection-box hidden-box" href="{{url("/teema/".$row->id)}}" style="display: none;"><p>{{$row->name}}</p></a>
            @endforeach
        </div>
    </div>
</div>
</body>
<script>
    $(document).ready(function() {
        $('#page-fade').fadeIn(1200);
        fadeInBoxes();
    });

    var curBox = 0;
    function fadeInBoxes() {
        setTimeout(function () {
            curBox++;
            if (curBox < {{sizeof($data['themes'])}} + 1) {
                $('#theme-box-'+curBox).fadeIn(1500);
                fadeInBoxes();
            }
        }, 100)
    }

</script>
</html>