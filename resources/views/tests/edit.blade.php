@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Muuda küsimust</div>

                    <div class="panel-body">
                        @if ($errors->count() > 0)
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form action="{{ route('tests.update', $test->id) }}" method="post">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            Küsimus:
                            <br />
                            <input class="form-control" type="text" name="question" value="{{ $test->question }}" />
                            <br />
                            Vastus 1:
                            <br />
                            <input class="form-control" type="text" name="choiceOne" value="{{ $test->choiceOne }}" />
                            <br />
                            Vastus 2:
                            <br />
                            <input class="form-control" type="text" name="choiceTwo" value="{{ $test->choiceTwo }}" />
                            <br />
                            Vastus 3:
                            <br />
                            <input class="form-control" type="text" name="choiceThree" value="{{ $test->choiceThree }}" />
                            <br />
                            Õige Vastus:
                            <br />
                            <input class="form-control" type="text" name="answer" value="{{ $test->answer }}" />
                            <br />
                            <select class="form-control" name="theme_id">
                                @foreach($data['themes'] as $row)
                                    @if($row->id == $test->theme_id)
                                        <option value="{{ $row ->id }}" selected="selected">{{ $row->name }}</option>
                                    @else
                                        <option value="{{ $row ->id }}">{{ $row->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <br />
                            <input class="form-control" type="submit" value="Submit" class="btn btn-default" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection