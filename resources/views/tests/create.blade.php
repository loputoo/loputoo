@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Lisa uus küsimus</div>

                    <div class="panel-body">
                        @if ($errors->count() > 0)
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form action="{{ route('tests.store') }}" method="post">
                            {{ csrf_field() }}
                            Küsimus:
                            <br />
                            <input class="form-control" id="" type="text" name="question" value="{{ old('question') }}" />
                            <br />
                            Vastus 1:
                            <br />
                            <input class="form-control" type="text" name="choiceOne" value="{{ old('choiceOne') }}" />
                            <br />
                            Vastus 2:
                            <br />
                            <input class="form-control" type="text" name="choiceTwo" value="{{ old('choiceTwo') }}" />
                            <br />
                            Vastus 3:
                            <br />
                            <input class="form-control" type="text" name="choiceThree" value="{{ old('choiceThree') }}" />
                            <br />
                            Õige Vastus:
                            <br />
                            <input class="form-control" type="text" name="answer" value="{{ old('answer') }}" />
                            <br />
                            <select class="form-control" name="theme_id">
                                @foreach($data['themes'] as $row)
                                    <option value="{{ $row ->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                            <br />
                            <input class="form-control" type="submit" value="Submit" class="btn btn-default" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection