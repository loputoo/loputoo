@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (session('message'))
                    <div class="alert alert-info">{{ session('message') }}</div>
                @endif
                <a href="{{ route('tests.create') }}" class="btn btn-default">Lisa uus küsimus</a>
                <div class="panel panel-default">
                    <div class="panel-heading">Testid</div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="checkbox_all">
                                </th>
                                <th>Küsimus</th>
                                <th>Esimene vastus</th>
                                <th>Teine vastus</th>
                                <th>Kolmas vastus</th>
                                <th>Õige vastus</th>
                                <th>Kategooria</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($tests as $test)
                                <tr>
                                    <td><input type="checkbox" class="checkbox_delete"
                                               name="entries_to_delete[]" value="{{ $test->id }}" /></td>

                                    <td>{{ $test->question }}</td>
                                    <td>{{ $test->choiceOne }}</td>
                                    <td>{{ $test->choiceTwo }}</td>
                                    <td>{{ $test->choiceThree }}</td>
                                    <td>{{ $test->answer }}</td>

                                    <td>{{ $data['themes'][$test->theme_id-1]->name }}</td>
                                    <td><!-- Edit and Delete Buttons -->
                                        <a href="{{ route('tests.edit', $test->id) }}" class="btn btn-default">Muuda</a>
                                        <form action="{{ route('tests.destroy', $test->id) }}" method="POST"
                                              style="display: inline"
                                              onsubmit="return confirm('Kas sa oled kindel?');">
                                            <input type="hidden" name="_method" value="DELETE">
                                            {{ csrf_field() }}
                                            <button class="btn btn-danger">Kustuta</button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">Kirjeid ei leitud</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <form action="{{ route('tests.mass_destroy') }}" method="post"
                              onsubmit="return confirm('Kas sa oled kindel?');">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="ids" id="ids" value="" />
                            <input type="submit" value="Kustuta valitud" class="btn btn-danger" />
                        </form>
                        {{ $tests->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(".checkbox_all").click(function(){
            $('input.checkbox_delete').prop('checked', this.checked);
        });
        function getIDs()
        {
            var ids = [];
            $('.checkbox_delete').each(function () {
                if($(this).is(":checked")) {
                    ids.push($(this).val());
                }
            });
            $('#ids').val(ids.join());
        }

        $(".checkbox_all").click(function(){
            $('input.checkbox_delete').prop('checked', this.checked);
            getIDs();
        });

        $('.checkbox_delete').change(function() {
            getIDs();
        });
    </script>
@endsection