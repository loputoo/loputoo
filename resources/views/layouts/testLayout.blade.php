<div class="row">
    <form id="start-test-form" class="col-12" name="see" method="post" action="javascript:getTests({{$data['id']}})">
        <p>Järgnev test kuvab ette 5 suvalist teemakohast küsimust.<br>
            Igale küsimusel on ainult 1 õige vastus.<br>
            Küsimused ja vastused on alati segatud.
        </p>
        {{csrf_field()}}
        <input type="submit" value="Alusta" class="btn-success test-start">
    </form>
    <form id="tests-form" class="col-12" name="see" method="post" action="javascript:sendTestData()" style="display: none;">
        {{csrf_field()}}
    </form>
</div>