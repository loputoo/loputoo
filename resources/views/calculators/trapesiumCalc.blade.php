<div class="row col-lg-10 offset-lg-1">
    <div class="panel panel-default col-md-3 border-bottom border-left border-top">
        <div class="panel-heading">
            <h3 class="panel-title text-center" style="margin-top: 10px !important;">Andmed</h3>
        </div>
        <div class="panel-body">
            <form id="calculator-data5" method="post" action="javascript:sendCalculatorData('{{url("/teema/".$data['id']."/calc/trapesium")}}', 5);">
                <div class="form-group">
                    <span><b>Küljed</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="a" placeholder="a"><br>
                    <input class="col-sm-12 calc-input-field" type="text" name="b" placeholder="b"><br>
                    <input class="col-sm-12 calc-input-field" type="text" name="c" placeholder="c"><br>
                    <input class="col-sm-12 calc-input-field" type="text" name="d" placeholder="d"><br><br>
                    <span><b>Kõrgus</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="h" placeholder="h"><br><br>
                    <span><b>Kesklõik</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="k" placeholder="k"><br><br>
                    <span><b>Nurgad</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="alp" placeholder="&ang;A"><br>
                    <input class="col-sm-12 calc-input-field" type="text" name="bet" placeholder="&ang;B"><br>
                    <input class="col-sm-12 calc-input-field" type="text" name="gam" placeholder="&ang;C"><br>
                    <input class="col-sm-12 calc-input-field" type="text" name="del" placeholder="&ang;D"><br>
                </div>
                {{csrf_field()}}
                <input class="col-sm-12 btn-success" type="submit" name="arvuta" value="Lahenda" style="margin-bottom: 10px;"/>
            </form>
        </div>
    </div>
    <div class="panel panel-default col-md-9 border-bottom border-right border-top">
        <div class="panel-heading">
            <h3 class="panel-title text-center " style="margin-top: 10px !important;">Mõõtmed</h3>
        </div>
        <div class="panel-body">
            <div id="disableOnCalc5" class="text-center">Sisestage andmed ning vajutage nupule "Lahenda".</div>
            <table id="enableOnCalc5"class="table table-bordered">
                <tbody>
                <tr>
                    <td>
                        Küljed
                    </td>
                    <td>
                        <p id="co_a5" class="calc-output-field"></p>
                        <p id="co_b5" class="calc-output-field"></p>
                        <p id="co_c5" class="calc-output-field"></p>
                        <p id="co_d5" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Kõrgus
                    </td>
                    <td>
                        <p id="co_h5" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Kesklõik
                    </td>
                    <td>
                        <p id="co_k5" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Ümbermõõt
                    </td>
                    <td>
                        <p id="co_p5" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Pindala
                    </td>
                    <td>
                        <p id="co_s5" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>Nurgad</td>
                    <td>
                        <p id="co_alp5" class="calc-output-field"></p>
                        <p id="co_bet5" class="calc-output-field"></p>
                        <p id="co_gam5" class="calc-output-field"></p>
                        <p id="co_del5" class="calc-output-field"></p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row col-lg-10 offset-lg-1">
    <div class="panel panel-default col-md-12  border-bottom border-left border-right">
        <div class="panel-heading">
            <h3 class="panel-title text-center" style="margin-top: 10px !important;">Lahenduskäik</h3>
        </div>
        <div id="calc-result5" class="panel-body text-center"></div>
    </div>
</div>