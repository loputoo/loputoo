<div class="row col-lg-10 offset-lg-1">
    <div class="panel panel-default col-md-3 border-bottom border-left border-top">
        <div class="panel-heading">
            <h3 class="panel-title text-center" style="margin-top: 10px !important;">Andmed</h3>
        </div>
        <div class="panel-body">
            <form id="calculator-data3" method="post" action="javascript:sendCalculatorData('{{url("/teema/".$data['id']."/calc/rhombus")}}', 3);">
                <div class="form-group">
                    <span><b>Küljed</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="a" placeholder="a"><br><br>
                    <span><b>Kõrgus</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="h" placeholder="h"><br><br>
                    <span><b>Diagonaalid</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="d1" placeholder="d&#8321;"><br>
                    <input class="col-sm-12 calc-input-field" type="text" name="d2" placeholder="d&#8322;"><br>
                </div>
                {{csrf_field()}}
                <input class="col-sm-12 btn-success" type="submit" name="arvuta" value="Lahenda" style="margin-bottom: 10px;"/>
            </form>
        </div>
    </div>
    <div class="panel panel-default col-md-9 border-bottom border-right border-top">
        <div class="panel-heading">
            <h3 class="panel-title text-center " style="margin-top: 10px !important;">Mõõtmed</h3>
        </div>
        <div class="panel-body">
            <div id="disableOnCalc3" class="text-center">Sisestage andmed ning vajutage nupule "Lahenda".</div>
            <table id="enableOnCalc3"class="table table-bordered">
                <tbody>
                <tr>
                    <td>
                        Küljed
                    </td>
                    <td>
                        <p id="co_a3" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Kõrgus
                    </td>
                    <td>
                        <p id="co_h3" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Diagonaalid
                    </td>
                    <td>
                        <p id="co_d13" class="calc-output-field"></p>
                        <p id="co_d23" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Ümbermõõt
                    </td>
                    <td>
                        <p id="co_p3" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Pindala
                    </td>
                    <td>
                        <p id="co_s3" class="calc-output-field"></p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row col-lg-10 offset-lg-1">
    <div class="panel panel-default col-md-12  border-bottom border-left border-right">
        <div class="panel-heading">
            <h3 class="panel-title text-center" style="margin-top: 10px !important;">Lahenduskäik</h3>
        </div>
        <div id="calc-result3" class="panel-body text-center"></div>
    </div>
</div>