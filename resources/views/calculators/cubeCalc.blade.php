<div class="row col-lg-10 offset-lg-1">
    <div class="panel panel-default col-md-3 border-bottom border-left border-top">
        <div class="panel-heading">
            <h3 class="panel-title text-center" style="margin-top: 10px !important;">Andmed</h3>
        </div>
        <div class="panel-body">
            <form id="calculator-data1" method="post" action="javascript:sendCalculatorData('{{url("/teema/".$data['id']."/calc/cube")}}', 1);">
                <div class="form-group">
                    <span><b>Külg</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="a" placeholder="a"><br>
                </div>
                {{csrf_field()}}
                <input class="col-sm-12 btn-success" type="submit" name="arvuta" value="Lahenda" style="margin-bottom: 10px;"/>
            </form>
        </div>
    </div>
    <div class="panel panel-default col-md-9 border-bottom border-right border-top">
        <div class="panel-heading">
            <h3 class="panel-title text-center " style="margin-top: 10px !important;">Mõõtmed</h3>
        </div>
        <div class="panel-body">
            <div id="disableOnCalc1" class="text-center">Sisestage andmed ning vajutage nupule "Lahenda".</div>
            <table id="enableOnCalc1"class="table table-bordered">
                <tbody>
                <tr>
                    <td>
                        Külg
                    </td>
                    <td>
                        <p id="co_a1" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Täispindala
                    </td>
                    <td>
                        <p id="co_st1" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Ruumala
                    </td>
                    <td>
                        <p id="co_v1" class="calc-output-field"></p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row col-lg-10 offset-lg-1">
    <div class="panel panel-default col-md-12  border-bottom border-left border-right">
        <div class="panel-heading">
            <h3 class="panel-title text-center" style="margin-top: 10px !important;">Lahenduskäik</h3>
        </div>
        <div id="calc-result1" class="panel-body text-center"></div>
    </div>
</div>