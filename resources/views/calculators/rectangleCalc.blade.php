<div class="row col-lg-10 offset-lg-1">
    <div class="panel panel-default col-md-3 border-bottom border-left border-top">
        <div class="panel-heading">
            <h3 class="panel-title text-center" style="margin-top: 10px !important;">Andmed</h3>
        </div>
        <div class="panel-body">
            <form id="calculator-data2" method="post" action="javascript:sendCalculatorData('{{url("/teema/".$data['id']."/calc/rectangle")}}', 2);">
                <div class="form-group">
                    <span><b>Küljed</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="a" placeholder="a"><br>
                    <input class="col-sm-12 calc-input-field" type="text" name="b" placeholder="b"><br><br>
                    <span><b>Diagonaal</b></span>
                    <input class="col-sm-12 calc-input-field" type="text" name="d" placeholder="d"><br>
                </div>
                {{csrf_field()}}
                <input class="col-sm-12 btn-success" type="submit" name="arvuta" value="Lahenda" style="margin-bottom: 10px;"/>
            </form>
        </div>
    </div>
    <div class="panel panel-default col-md-9 border-bottom border-right border-top">
        <div class="panel-heading">
            <h3 class="panel-title text-center " style="margin-top: 10px !important;">Mõõtmed</h3>
        </div>
        <div class="panel-body">
            <div id="disableOnCalc2" class="text-center">Sisestage andmed ning vajutage nupule "Lahenda".</div>
            <table id="enableOnCalc2"class="table table-bordered">
                <tbody>
                <tr>
                    <td>
                        Küljed
                    </td>
                    <td>
                        <p id="co_a2" class="calc-output-field"></p>
                        <p id="co_b2" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Diagonaal
                    </td>
                    <td>
                        <p id="co_d12" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Ümbermõõt
                    </td>
                    <td>
                        <p id="co_p2" class="calc-output-field"></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Pindala
                    </td>
                    <td>
                        <p id="co_s2" class="calc-output-field"></p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row col-lg-10 offset-lg-1">
    <div class="panel panel-default col-md-12  border-bottom border-left border-right">
        <div class="panel-heading">
            <h3 class="panel-title text-center" style="margin-top: 10px !important;">Lahenduskäik</h3>
        </div>
        <div id="calc-result2" class="panel-body text-center"></div>
    </div>
</div>