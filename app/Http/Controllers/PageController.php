<?php
/**
 * Created by PhpStorm.
 * User: B50-50
 * Date: 10.04.2018
 * Time: 11:45
 */

namespace App\Http\Controllers;

use App\Classes\calc_paralleloPrism;
use App\Classes\calc_rectangular;
use App\Classes\calc_uprightPrism;
use DB;
use App\Test;
use App\Classes\calc_triangle;
use App\Classes\calc_square;
use App\Classes\calc_rectangle;
use App\Classes\calc_rhombus;
use App\Classes\calc_parallelogram;
use App\Classes\calc_trapesium;
use App\Classes\calc_cube;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller {
    public function index() {
        $data = array('themes' => DB::table('teemads')->get());
        return view('welcome')->with('data', $data);
    }

    public function openTheme($themeid) {
        return view('teema')->with('data', array('themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid)));
    }

    public function triangleCalc($themeid) {
        $calc = new calc_triangle();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['a'], $_POST['b'], $_POST['c'], $_POST['h'], $_POST['alp'], $_POST['bet'], $_POST['gam']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function squareCalc($themeid) {
        $calc = new calc_square();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['a']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function rectangleCalc($themeid) {
        $calc = new calc_rectangle();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['a'], $_POST['b'], $_POST['d']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function rhombusCalc($themeid) {
        $calc = new calc_rhombus();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['a'], $_POST['h'], $_POST['d1'], $_POST['d2']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function parallelogramCalc($themeid) {
        $calc = new calc_parallelogram();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['a'], $_POST['b'], $_POST['h'], $_POST['alp'], $_POST['bet'], $_POST['gam'], $_POST['del']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function trapesiumCalc($themeid) {
        $calc = new calc_trapesium();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['a'], $_POST['b'], $_POST['c'], $_POST['d'], $_POST['k'], $_POST['h'], $_POST['alp'], $_POST['bet'], $_POST['gam'], $_POST['del']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function cubeCalc($themeid) {
        $calc = new calc_cube();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['a']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function rectangularCalc($themeid) {
        $calc = new calc_rectangular();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['a'], $_POST['b'], $_POST['c']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function uprightPrismCalc($themeid) {
        $calc = new calc_uprightPrism();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['p'], $_POST['sp'], $_POST['h']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function paralleloPrismCalc($themeid) {
        $calc = new calc_paralleloPrism();

        $data = array('arvuta'=> true, 'themes'=>$this->getThemes(), 'id'=>$this->getId($themeid), 'teemaNimi'=>$this->getName($themeid));
        $calcData = $calc->instantiate($_POST['a'], $_POST['b'], $_POST['h']);
        $returnData = array_merge($data, array('calcData'=>$calcData), array('tests', Test::get()));

        return response()->json($returnData);
    }

    public function getTests($themeid) {
        $tests=Test::where('theme_id', $themeid)->orderByRaw('RAND()')->take(5)->get();
        return response()->json(array('tests'=>$tests));
    }

    public function getResults() {
        $response = '';
        $total = 0;
        $right = 0;
        $score = '';

        for($i = 0; $i < 5; $i++) {
            if(isset($_POST['question'.$i])) {
               $explodedStr = explode(":", $_POST['question'.$i]);
               $question = DB::table('tests')->where('id', $explodedStr[0])->first();

               if($question->answer == $explodedStr[1]) {
                   $response.='<div id="right-result" class="col-12 test-result-row">';
                   $right++;
               } else {
                   $response.='<div id="wrong-result" class="col-12 test-result-row">';
               }
                $response.=$question->question.'<br>';
                $response.='Teie vastus: '.$explodedStr[1].'<br>';
                $response.='Õige vastus: '.$question->answer;
                $response.='</div>';

                $total++;
            } else {
                return response()->json(array('error' => 'Te ei vastanud kõikidele küsimustele.'));
            }
        }

        $score = '<div>Teie tulemus on ' . $right . '/' . $total . ' (' . (100 / $total * $right) . '%)</div><br>';

        $data = array('score' => $score, 'result'=> $response);
        return response()->json($data);
    }

    /**
     * Korduvad päringud
     */

    private function getThemes() {
        $themes = array('themes' => DB::table('teemads')->get());
        return $themes;
    }

    private function getId($themeid) {
        $theme = DB::table('teemads')->where('id', $themeid)->first();

        return $theme->id;
    }

    private function getName($themeid) {
        $theme = DB::table('teemads')->where('id', $themeid)->first();

        return $theme->name;
    }
}