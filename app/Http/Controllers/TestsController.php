<?php

namespace App\Http\Controllers;
use App\Test;
use DB;
use App\Http\Requests;
use App\Http\Requests\StoreTestRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\PageController;

class TestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tests=Test::paginate(10);
        $data = array('themes' => DB::table('teemads')->get());
        return view('tests.index', compact('tests'))->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = array('themes' => DB::table('teemads')->get());
        return view('tests.create')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTestRequest $request)
    {
        //
        Test::create($request->all());
        return redirect()->route('tests.index')->with(['message' => 'Küsimus lisatud']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $test = Test::findOrFail($id);
        $data = array('themes' => DB::table('teemads')->get());
        return view('tests.edit', compact('test'))->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $test = Test::findOrFail($id);
        $test->update($request->all());
        return redirect()->route('tests.index')->with(['message' => 'Küsimus uuendatud']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $test = Test::findorFail($id);
        $test->delete();
        return redirect()->route('tests.index')->with(['message'=>'Küsimus on kustutatud']);
    }
    public function massDestroy(Request $request)
    {
        $tests = explode(',', $request->input('ids'));
        foreach ($tests as $test_id) {
            $test = Test::findOrFail($test_id);
            $test->delete();
        }
        return redirect()->route('tests.index')->with(['message' => 'Küsimused on kustutatud']);
    }
}
