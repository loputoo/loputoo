<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    //
    protected $fillable = ['question', 'choiceOne', 'choiceTwo', 'choiceThree', 'answer', 'theme_id'];
    public $timestamps = false;
}
