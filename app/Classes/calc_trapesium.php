<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/24/2018
 * Time: 7:48 PM
 */

namespace App\Classes;


class calc_trapesium {
    public function instantiate($a, $b, $c, $d, $k, $h, $alp, $bet, $gam, $del) {
        $result = '';
        $s = 0;
        $p = 0;

        if(!is_numeric($a) && !is_numeric($b) && !is_numeric($c) && !is_numeric($d) && !is_numeric($k) && !is_numeric($h) && !is_numeric($alp) && !is_numeric($bet) && !is_numeric($gam) && !is_numeric($del)) {
            return array('error' => 'Trapetsi lahendamiseks sisestage andmed.');
        }

        $result .= 'Ümbermõõdu leidmine.<br>';
        $result .= 'P = 2 * (a + b)<br>';
        $result .= 'P = '.$this->umbermoot($a, $b, $c, $d).'<br><br>';
        $p = $this->umbermoot($a, $b, $c, $d);
        if(!is_numeric($p)) {
            return array('error'=> 'Trapetsi ümbermõõdu lahendamiseks on vaja teada kõiki külgi.');
        }

        if(!is_numeric($k)) {
            $result .= 'Ristlõike leidmine.<br>';
            $result .= 'k = (a + b) / 2<br>';
            $result .= 'k = '.number_format(($a + $b) / 2, 2).'<br><br>';
            $k = number_format(($a + $b) / 2, 2);
        }

        if(!is_numeric($h)) {
            $result .= 'Kõrguse leidmine.<br>';
            $result .= '1. Tuleb pikemast alusest lahutada lühem.<br>';
            $result .= 'a<sub>1</sub> = (a - b) / 2<br>';
            $result .= '2. Leiame kõrguse(Pythagorase teoreem)<br>';
            $result .= 'h = &Sqrt;(a<sub>1</sub><sup>2</sup> + c<sup>2</sup>)<br>';
            $result .= 'h = '.number_format(sqrt(pow(abs($a - $b) / 2, 2) + pow($c, 2)), 2).'<br><br>';
            $h = number_format(sqrt(pow(abs($a - $b) / 2, 2) + pow($c, 2)), 2);
        }

        $result .= 'Pindala leidmine.<br>';
        $result .= 'S = k * h ('.$k.'*'.$h.')<br>';
        $result .= 'S = ' . $this->pindala($k, $h) . '<br><br>';
        $s = $this->pindala($k, $h);
        if(!is_numeric($s)) {
            return array('error'=> 'Trapetsi pindala lahendamiseks on vaja teada ristlõiget ja kõrgust.');
        }

        if(is_numeric($alp) || is_numeric($bet) || is_numeric($gam) || is_numeric($del)) {

            if (is_numeric($alp) && !is_numeric($bet)) {
                $result .= '&ang;B leidmine.<br>';
                $result .= '&ang;B = 180&deg; - &ang;A<br>';
                $result .= '&ang;B = ' . (180 - $alp) . '&deg;<br><br>';
                $bet = 180 - $alp;
            }

            if(!is_numeric($alp) && is_numeric($bet)) {
                $result .= '&ang;A leidmine.<br>';
                $result .= '&ang;A = 180&deg; - &ang;B<br>';
                $result .= '&ang;A = ' . (180 - $bet) . '&deg;<br><br>';
                $alp = 180 - $bet;
            }

            if (is_numeric($bet) && !is_numeric($gam)) {
                $result .= '&ang;C leidmine.<br>';
                $result .= '&ang;C = 180&deg; - &ang;B<br>';
                $result .= '&ang;C = ' . (180 - $bet) . '&deg;<br><br>';
                $gam = 180 - $bet;
            }

            if (!is_numeric($bet) && is_numeric($gam)) {
                $result .= '&ang;B leidmine.<br>';
                $result .= '&ang;B = 180&deg; - &ang;C<br>';
                $result .= '&ang;B = ' . (180 - $gam) . '&deg;<br><br>';
                $bet = 180 - $gam;
            }

            if (is_numeric($gam) && !is_numeric($del)) {
                $result .= '&ang;D leidmine.<br>';
                $result .= '&ang;D = 180&deg; - &ang;C<br>';
                $result .= '&ang;D = ' . (180 - $gam) . '&deg;<br><br>';
                $del = 180 - $gam;
            }

            if (!is_numeric($gam) && is_numeric($del)) {
                $result .= '&ang;C leidmine.<br>';
                $result .= '&ang;C = 180&deg; - &ang;D<br>';
                $result .= '&ang;C = ' . (180 - $del) . '&deg;<br><br>';
                $gam = 180 - $del;
            }

            if (is_numeric($del) && !is_numeric($alp)) {
                $result .= '&ang;A leidmine.<br>';
                $result .= '&ang;A = 180&deg; - &ang;D<br>';
                $result .= '&ang;A = ' . (180 - $del) . '&deg;<br><br>';
                $alp = 180 - $del;
            }

            if (!is_numeric($del) && is_numeric($alp)) {
                $result .= '&ang;D leidmine.<br>';
                $result .= '&ang;D = 180&deg; - &ang;A<br>';
                $result .= '&ang;D = ' . (180 - $alp) . '&deg;<br><br>';
                $del = 180 - $alp;
            }
        }

        return $this->output($a, $b, $c, $d, $k, $h, $s, $p, $alp, $bet, $gam, $del, $result);
    }

    public function umbermoot($a, $b, $c, $d) {
        $p = 'undefined';

        if(is_numeric($a) && is_numeric($b) && is_numeric($c) && is_numeric($d)) {
            return $a + $b + $c + $d;
        }

        return $p;
    }

    public function pindala($k, $h) {
        $s = 'undefined';

        if(is_numeric($k) && is_numeric($h)) {
            $s = $k * $h;
        }

        return $s;
    }

    public function output($a, $b, $c, $d, $k, $h, $s, $p, $alp, $bet, $gam, $del, $result)  {
        is_numeric($a) ? $data['a'] = $a : $data['a'] = '-';
        is_numeric($b) ? $data['b'] = $b : $data['b'] = '-';
        is_numeric($c) ? $data['c'] = $c : $data['c'] = '-';
        is_numeric($d) ? $data['d'] = $d : $data['d'] = '-';
        is_numeric($k) ? $data['k'] = $k : $data['k'] = '-';
        is_numeric($h) ? $data['h'] = $h : $data['h'] = '-';
        is_numeric($s) ? $data['s'] = $s : $data['s'] = '-';
        is_numeric($p) ? $data['p'] = $p : $data['p'] = '-';
        is_numeric($alp) ? $data['alp'] = $alp : $data['alp'] = '-';
        is_numeric($bet) ? $data['bet'] = $bet : $data['bet'] = '-';
        is_numeric($gam) ? $data['gam'] = $gam : $data['gam'] = '-';
        is_numeric($del) ? $data['del'] = $del : $data['del'] = '-';
        $data['result'] = $result;
        return $data;
    }
}