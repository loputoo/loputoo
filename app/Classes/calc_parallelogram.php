<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/24/2018
 * Time: 7:48 PM
 */

namespace App\Classes;


class calc_parallelogram {
    public function instantiate($a, $b, $h, $alp, $bet, $gam, $del) {
        $result = '';
        $s = '';
        $p = '';
        if(!is_numeric($a) && !is_numeric($b) && !is_numeric($h) && !is_numeric($alp) && !is_numeric($bet) && !is_numeric($gam) && !is_numeric($del)) {
            return array('error' => 'Rööpküliku lahendamiseks sisestage andmed.');
        }

        $result .= 'Ümbermõõdu leidmine.<br>';
        $result .= 'P = 2 * (a + b)<br>';
        $result .= 'P = '.$this->umbermoot($a, $b).'<br><br>';
        $p = $this->umbermoot($a, $b);
        if(!is_numeric($p)) {
            return array('error'=> 'Rööpküliku ümbermõõdu lahendamiseks on vaja teada mõlemat külge.');
        }

        $result .= 'Pindala leidmine.<br>';
        if(is_numeric($a) && is_numeric($b)) {
            $result .= 'S = a * b<br>';
        } else if(is_numeric($a) && is_numeric($h)) {
            $result .= 'S = a * h<br>';
        } else {
            return array('error'=> 'Rööpküliku pindala lahendamiseks on vaja teada mõlemat külge või alust ja kõrgust.');
        }
        $result .= 'S = '.$this->pindala($a, $b, $h).'<br><br>';
        $s = $this->pindala($a, $b, $h);

        if(is_numeric($alp) || is_numeric($bet) || is_numeric($gam) || is_numeric($del)) {

            if (is_numeric($alp) && !is_numeric($bet)) {
                $result .= '&ang;B leidmine.<br>';
                $result .= '&ang;B = 180&deg; - &ang;A<br>';
                $result .= '&ang;B = ' . (180 - $alp) . '&deg;<br><br>';
                $bet = 180 - $alp;
            }

            if(!is_numeric($alp) && is_numeric($bet)) {
                $result .= '&ang;A leidmine.<br>';
                $result .= '&ang;A = 180&deg; - &ang;B<br>';
                $result .= '&ang;A = ' . (180 - $bet) . '&deg;<br><br>';
                $alp = 180 - $bet;
            }

            if (is_numeric($bet) && !is_numeric($gam)) {
                $result .= '&ang;C leidmine.<br>';
                $result .= '&ang;C = 180&deg; - &ang;B<br>';
                $result .= '&ang;C = ' . (180 - $bet) . '&deg;<br><br>';
                $gam = 180 - $bet;
            }

            if (!is_numeric($bet) && is_numeric($gam)) {
                $result .= '&ang;B leidmine.<br>';
                $result .= '&ang;B = 180&deg; - &ang;C<br>';
                $result .= '&ang;B = ' . (180 - $gam) . '&deg;<br><br>';
                $bet = 180 - $gam;
            }

            if (is_numeric($gam) && !is_numeric($del)) {
                $result .= '&ang;D leidmine.<br>';
                $result .= '&ang;D = 180&deg; - &ang;C<br>';
                $result .= '&ang;D = ' . (180 - $gam) . '&deg;<br><br>';
                $del = 180 - $gam;
            }

            if (!is_numeric($gam) && is_numeric($del)) {
                $result .= '&ang;C leidmine.<br>';
                $result .= '&ang;C = 180&deg; - &ang;D<br>';
                $result .= '&ang;C = ' . (180 - $del) . '&deg;<br><br>';
                $gam = 180 - $del;
            }

            if (is_numeric($del) && !is_numeric($alp)) {
                $result .= '&ang;A leidmine.<br>';
                $result .= '&ang;A = 180&deg; - &ang;D<br>';
                $result .= '&ang;A = ' . (180 - $del) . '&deg;<br><br>';
                $alp = 180 - $del;
            }

            if (!is_numeric($del) && is_numeric($alp)) {
                $result .= '&ang;D leidmine.<br>';
                $result .= '&ang;D = 180&deg; - &ang;A<br>';
                $result .= '&ang;D = ' . (180 - $alp) . '&deg;<br><br>';
                $del = 180 - $alp;
            }
        }

        return $this->output($a, $b, $h, $s, $p, $alp, $bet, $gam, $del, $result);
    }

    public function umbermoot($a, $b) {
        $p = 'undefined';

        if(is_numeric($a) && is_numeric($b)) {
            return 2 * ($a + $b);
        }

        return $p;
    }

    public function pindala($a, $b, $h) {
        $s = 'undefined';

        if(is_numeric($a) && is_numeric($h)) {
            $s = $a * $h;
        }

        if(is_numeric($a) && is_numeric($b)) {
            $s = $a * $b;
        }

        return $s;
    }

    public function output($a, $b, $h, $s, $p, $alp, $bet, $gam, $del, $result)  {
        is_numeric($a) ? $data['a'] = $a : $data['a'] = '-';
        is_numeric($b) ? $data['b'] = $b : $data['b'] = '-';
        is_numeric($h) ? $data['h'] = $h : $data['h'] = '-';
        is_numeric($s) ? $data['s'] = $s : $data['s'] = '-';
        is_numeric($p) ? $data['p'] = $p : $data['p'] = '-';
        is_numeric($alp) ? $data['alp'] = $alp : $data['alp'] = '-';
        is_numeric($bet) ? $data['bet'] = $bet : $data['bet'] = '-';
        is_numeric($gam) ? $data['gam'] = $gam : $data['gam'] = '-';
        is_numeric($del) ? $data['del'] = $del : $data['del'] = '-';
        $data['result'] = $result;
        return $data;
    }
}