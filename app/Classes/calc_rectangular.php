<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/18/2018
 * Time: 6:21 PM
 */

namespace App\Classes;


class calc_rectangular {
    public function instantiate($a, $b, $c) {
        $result = '';

        if(!is_numeric($a ) && !is_numeric($b) && !is_numeric($c)) {
            return array('error' => 'Risttahuka lahendamiseks sisestage andmed!');
        }

        $result .= 'Täispindala leidmine.<br>';
        $result .= 'St = 2 * (a*b + a*c + b*c)<br>';
        $result .= 'St = '.$this->pindalaT($a, $b, $c).'<sup>2</sup><br><br>';
        $st = $this->pindalaT($a, $b, $c);
        if(!is_numeric($st)) {
            return array('error' => 'Risttahuka täispindala lahendamiseks sisestage kõik küljed!');
        }

        $result .= 'Ruumala leidmine.<br>';
        $result .= 'V = a*b*c<br>';
        $result .= 'V = '.$this->ruumala($a, $b, $c).'<sup>3</sup><br><br>';
        $v = $this->ruumala($a, $b, $c);
        if(!is_numeric($v)) {
            return array('error' => 'Risttahuka ruumala lahendamiseks sisestage kõik küljed!');
        }

        return $this->output($a, $b, $c, $st, $v, $result);
    }

    function pindalaT($a, $b, $c) {
        $st = 'undefined';

        if(is_numeric($a) && is_numeric($b) && is_numeric($c)) {
            $st = 2 * ($a*$b + $a*$c + $b*$c);
        }

        return $st;
    }

    function ruumala($a, $b, $c) {
        $v = 'undefined';

        if(is_numeric($a) && is_numeric($b) && is_numeric($c)) {
            $v = $a * $b * $c;
        }

        return $v;
    }

    function output($a, $b, $c, $st, $v, $result)  {
        is_numeric($a) ? $data['a'] = $a : $data['a'] = '-';
        is_numeric($b) ? $data['b'] = $b : $data['b'] = '-';
        is_numeric($c) ? $data['c'] = $c : $data['c'] = '-';
        is_numeric($st) ? $data['st'] = $st : $data['st'] = '-';
        is_numeric($v) ? $data['v'] = $v : $data['v'] = '-';
        $data['result'] = $result;
        return $data;
    }
}