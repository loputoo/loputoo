<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/18/2018
 * Time: 6:21 PM
 */

namespace App\Classes;


class calc_rhombus
{
    public function instantiate($a, $h, $d1, $d2) {
        $result = '';

        if(!is_numeric($a) && !is_numeric($h) && !is_numeric($d1) && !is_numeric($d2)) {
            return array('error' => 'Rombi lahendamiseks peate sisestama vajalikud andmed.');
        }

        if((is_numeric($d1) && is_numeric($d2)) && (!is_numeric($a) && !is_numeric($h))) {
            $result .= 'Külje a leidmine diagonaalide kaudu(Pythagorase teoreem).<br>';
            $result .= 'a = &Sqrt;[(d<sub>1</sub>/2)<sup>2</sup> + (d<sub>2</sub>/2)<sup>2</sup>]<br>';
            $result .= 'a = '.number_format(sqrt(pow($d1/2, 2) + pow($d2/2, 2)), 2).'<br><br>';
            $a = number_format(sqrt(pow($d1/2, 2) + pow($d2/2, 2)), 2);

            $result .= 'Ümbermõõdu leidmine<br>';
            $result .= 'P = 4a<br>';
            $result .= 'P = '.$this->umbermoot($a).'<br><br>';
            $p = $this->umbermoot($a);

            $result .= 'Pindala leidmine<br>';
            $result .= 'S = (d<sub>1</sub>*d<sub>2</sub>)/2<br>';
            $result .= 'S = '.(($d1*$d2)/2).'<br><br>';
            $s = ($d1*$d2) / 2;

            $result .= 'Kõrguse leidmine pindala kaudu.<br>';
            $result .= 'S = a*h => h=S/a<br>';
            $result .= 'h = '.number_format($s / $a, 2).'<br><br>';
            $h = number_format($s / $a, 2);

            return $this->output($a, $h, $d1, $d2, $p, $s, $result);

        }
        else if((is_numeric($a) && is_numeric($h)) && (!is_numeric($d1) && !is_numeric($d2))) {
            $result .= 'Ümbermõõdu leidmine<br>';
            $result .= 'P = 4a<br>';
            $result .= 'P = '.$this->umbermoot($a).'<br><br>';
            $p = $this->umbermoot($a);

            $result .= 'Pindala leidmine.<br>';
            $result .= 'S = a*h<br>';
            $result .= 'S = '.$a*$h.'<br>';
            $s = $a*$h;

            return $this->output($a, $h, $d1, $d2, $p, $s, $result);

        }
        else if((is_numeric($a) && is_numeric($d1)) && (!is_numeric($h) && !is_numeric($d2))) {
            $result .= 'Ümbermõõdu leidmine<br>';
            $result .= 'P = 4a<br>';
            $result .= 'P = '.$this->umbermoot($a).'<br><br>';
            $p = $this->umbermoot($a);

            $result .= 'Teise diagonaali leidmine<br>';
            $result .= 'd<sub>2</sub> = 2(&Sqrt;((d<sub>1</sub>/2)<sup>2</sup> + a<sup>2</sup>))<br>';
            $result .= 'd<sub>2</sub> = '.number_format((2 * sqrt(pow($d1 / 2, 2) + pow($a, 2))), 2).'<br><br>';
            $d2 = number_format((2 * sqrt(pow($d1 / 2, 2) + pow($a, 2))), 2);

            $result .= 'Pindala leidmine<br>';
            $result .= 'S = (d<sub>1</sub>*d<sub>2</sub>)/2<br>';
            $result .= 'S = '.(($d1*$d2)/2).'<br><br>';
            $s = ($d1*$d2) / 2;

            $result .= 'Kõrguse leidmine pindala kaudu.<br>';
            $result .= 'S = a*h => h=S/a<br>';
            $result .= 'h = '.$s / $a.'<br><br>';
            $h = $s / $a;

            return $this->output($a, $h, $d1, $d2, $p, $s, $result);

        }
        else if((is_numeric($a) && is_numeric($d2)) && (!is_numeric($h) && !is_numeric($d1))) {
            $result .= 'Ümbermõõdu leidmine<br>';
            $result .= 'P = 4a<br>';
            $result .= 'P = '.$this->umbermoot($a).'<br><br>';
            $p = $this->umbermoot($a);

            $result .= 'Teise diagonaali leidmine<br>';
            $result .= 'd<sub>1</sub> = 2(&Sqrt;((d<sub>2</sub>/2)<sup>2</sup> + a<sup>2</sup>))<br>';
            $result .= 'd<sub>1</sub> = '.number_format((2 * sqrt(pow($d2 / 2, 2) + pow($a, 2))), 2).'<br><br>';
            $d2 = number_format((2 * sqrt(pow($d2 / 2, 2) + pow($a, 2))), 2);

            $result .= 'Pindala leidmine<br>';
            $result .= 'S = (d<sub>1</sub>*d<sub>2</sub>)/2<br>';
            $result .= 'S = '.(($d1*$d2)/2).'<br><br>';
            $s = ($d1*$d2) / 2;

            $result .= 'Kõrguse leidmine pindala kaudu.<br>';
            $result .= 'S = a*h => h=S/a<br>';
            $result .= 'h = '.$s / $a.'<br><br>';
            $h = $s / $a;

            return $this->output($a, $h, $d1, $d2, $p, $s, $result);


        }
        else if((is_numeric($a) && is_numeric($h) && is_numeric($d1)) && !is_numeric($d2)) {
            $result .= 'Ümbermõõdu leidmine<br>';
            $result .= 'P = 4a<br>';
            $result .= 'P = '.$this->umbermoot($a).'<br><br>';
            $p = $this->umbermoot($a);

            $result .= 'Pindala leidmine.<br>';
            $result .= 'S = a*h<br>';
            $result .= 'S = '.$a*$h.'<br>';
            $s = $a*$h;

            $result .= 'Teise diagonaali d<sub>2</sub> leidmine pindala kaudu.<br>';
            $result .= 'd2 = (d<sub>1</sub>*d<sub>2</sub>)/2  => d<sub>2</sub>=2S/d<sub>1</sub><br>';
            $result .= 'd2 = '.($s*2) / $d1.'<br><br>';
            $d2 = ($s*2) / $d1;

            return $this->output($a, $h, $d1, $d2, $p, $s, $result);

        }
        else if((is_numeric($a) && is_numeric($h) && is_numeric($d2)) && !is_numeric($d1)) {
            $result .= 'Ümbermõõdu leidmine<br>';
            $result .= 'P = 4a<br>';
            $result .= 'P = '.$this->umbermoot($a).'<br><br>';
            $p = $this->umbermoot($a);

            $result .= 'Pindala leidmine.<br>';
            $result .= 'S = a*h<br>';
            $result .= 'S = '.$a*$h.'<br>';
            $s = $a*$h;

            $result .= 'Teise diagonaali d<sub>2</sub> leidmine pindala kaudu.<br>';
            $result .= 'd1 = (d<sub>1</sub>*d<sub>2</sub>)/2  => d<sub>1</sub>=2S/d<sub>2</sub><br>';
            $result .= 'd1 = '.($s*2) / $d1.'<br><br>';
            $d1 = ($s*2) / $d1;

            return $this->output($a, $h, $d1, $d2, $p, $s, $result);

        }
        else if(is_numeric($a) && (is_numeric($d1) && is_numeric($d2) && !is_numeric($h))) {
            $result .= 'Ümbermõõdu leidmine<br>';
            $result .= 'P = 4a<br>';
            $result .= 'P = '.$this->umbermoot($a).'<br><br>';
            $p = $this->umbermoot($a);

            $result .= 'Pindala leidmine<br>';
            $result .= 'S = (d<sub>1</sub>*d<sub>2</sub>)/2<br>';
            $result .= 'S = '.(($d1*$d2)/2).'<br><br>';
            $s = ($d1*$d2) / 2;

            $result .= 'Kõrguse leidmine pindala kaudu.<br>';
            $result .= 'S = a*h => h=S/a<br>';
            $result .= 'h = '.$s / $a.'<br><br>';
            $h = $s / $a;

            return $this->output($a, $h, $d1, $d2, $p, $s, $result);

        }
        else if(is_numeric($h) && (is_numeric($d1) && is_numeric($d2) && !is_numeric($a))) {
            $result .= 'Pindala leidmine<br>';
            $result .= 'S = (d<sub>1</sub>*d<sub>2</sub>)/2<br>';
            $result .= 'S = '.(($d1*$d2)/2).'<br><br>';
            $s = ($d1*$d2) / 2;

            $result .= 'Külje leidmine pindala kaudu.<br>';
            $result .= 'S = a*h => a=S/h<br>';
            $result .= 'a = '.$s / $h.'<br><br>';
            $a = $s / $h;

            $result .= 'Ümbermõõdu leidmine<br>';
            $result .= 'P = 4a<br>';
            $result .= 'P = '.$this->umbermoot($a).'<br><br>';
            $p = $this->umbermoot($a);

            return $this->output($a, $h, $d1, $d2, $p, $s, $result);

        }
        else {
            return array('error' => 'Sisestatud andmetega pole võimalik rombi lahendada.');
        }
    }

    public function umbermoot($a) {
        return $a * 4;
    }

    public function output($a, $h, $d1, $d2, $p, $s, $result)  {
        is_numeric($a) ? $data['a'] = $a : $data['a'] = '-';
        is_numeric($h) ? $data['h'] = $h : $data['h'] = '-';
        is_numeric($d1) ? $data['d1'] = $d1 : $data['d1'] = '-';
        is_numeric($d2) ? $data['d2'] = $d2 : $data['d2'] = '-';
        is_numeric($p) ? $data['p'] = $p : $data['p'] = '-';
        is_numeric($s) ? $data['s'] = $s : $data['s'] = '-';
        $data['result'] = $result;
        return $data;
    }
}