<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/15/2018
 * Time: 3:57 PM
 */

namespace App\Classes;

class calc_triangle {
    public function instantiate($a, $b, $c, $h, $alp, $bet, $gam) {
        $result = '';
        $anglesPosted = 0;
        $p = '';
        $s = '';

        if(!is_numeric($a) && !is_numeric($b) && !is_numeric($c) && !is_numeric($h) && !is_numeric($alp) && !is_numeric($bet) && !is_numeric($gam)) {
            return array('error' => 'Kolmnurga lahendamiseks on vaja kõiki külgi ja kõrgust või/ja kahte nurka!');
        }

        if(is_numeric($a) || is_numeric($b) || is_numeric($c) || is_numeric($h)) {
            if(is_numeric($a) && is_numeric($b) && is_numeric($c) && is_numeric($h)) {
                if($a+$b < $c || $a+$c < $b || $c+$b < $a) {
                    return array('error' => 'Kolmnurga kaks külge ei saa olla lühem kui kolmas külg!');
                }
                $result .= 'Ümbermõõdu leidmine<br>';
                $result .= 'P = a + b + c<br>';
                $result .= 'P = ' . $a . ' + ' . $b . ' + ' . $c . ' = ' . $this->umbermoot($a, $b, $c) . '<br><br>';
                $p = $this->umbermoot($a, $b, $c);

                $result .= 'Pindala leidmine<br>';
                $result .= 'S = a * h / 2<br>';
                $result .= 'S = ' . $a . ' * ' . $h . ' / 2 = ' . $this->pindala($a, $h) . '<br><br>';
                $s = $this->pindala($a, $h);
            } else {
                return array('error' => 'Kolmnurga lahendamiseks on vaja teada kõiki külgi ja kõrgust!');
            }
        }

        if(is_numeric($alp) || is_numeric($bet) || is_numeric($gam)) {
            if (!empty($alp)) {
                $anglesPosted++;
            }

            if (!empty($bet)) {
                $anglesPosted++;
            }

            if (!empty($gam)) {
                $anglesPosted++;
            }

            if (intval($anglesPosted) == 1) {
                return array('error' => 'Nurkade arvutamiseks on vaja teada vähemalt kahte nurka!');
            } else if(intval($anglesPosted) == 3) {
                if($alp+$bet+$gam != 180) {
                    return array('error' => 'Nurkade summa peab võrduma 180&deg;!');
                }
            } else {
                $result .= 'Kolmanda nurga leidmine<br>';
                if (!is_numeric($alp)) {
                    $result .= 'A = 180&deg; - B - C<br>';
                    $result .= 'A = 180&deg; - ' . $bet . '&deg; - ' . $gam . '&deg; = ' . (180 - $bet - $gam) . '&deg;<br>';
                    $alp = 180 - $bet - $gam;
                }
                if (!is_numeric($bet)) {
                    $result .= 'B = 180&deg; - A - C<br>';
                    $result .= 'B = 180&deg; - ' . $alp . '&deg; - ' . $gam . '&deg; = ' . (180 - $alp - $gam) . '&deg;<br>';
                    $bet = 180 - $alp - $gam;
                } else {
                    $result .= 'C = 180&deg; - A - B<br>';
                    $result .= 'C = 180&deg; - ' . $alp . '&deg; - ' . $bet . '&deg; = ' . (180 - $alp - $bet) . '&deg;<br>';
                    $gam = 180 - $alp - $bet;
                }
            }
        }

        return $this->output($a, $b, $c, $h, $p, $s, $alp, $bet, $gam, $result);
    }

    public function umbermoot($a, $b, $c) {
        return $a+$b+$c;
    }

    public function pindala($a, $h) {
        return ($a*$h) / 2;
    }

    function output($a, $b, $c, $h, $p, $s, $alp, $bet, $gam, $result)  {
        is_numeric($a) ? $data['a'] = $a : $data['a'] = '-';
        is_numeric($b) ? $data['b'] = $b : $data['b'] = '-';
        is_numeric($c) ? $data['c'] = $c : $data['c'] = '-';
        is_numeric($h) ? $data['h'] = $h : $data['h'] = '-';
        is_numeric($p) ? $data['p'] = $p : $data['p'] = '-';
        is_numeric($s) ? $data['s'] = $s : $data['s'] = '-';
        is_numeric($alp) ? $data['alp'] = $alp : $data['alp'] = '-';
        is_numeric($bet) ? $data['bet'] = $bet : $data['bet'] = '-';
        is_numeric($gam) ? $data['gam'] = $gam : $data['gam'] = '-';
        $data['result'] = $result;
        return $data;
    }
}