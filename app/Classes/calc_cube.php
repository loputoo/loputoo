<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/18/2018
 * Time: 6:21 PM
 */

namespace App\Classes;


class calc_cube
{
    public function instantiate($a) {
        $result = '';

        if(!is_numeric($a)) {
            return array('error' => 'Kuubi lahendamiseks sisestage andmed!');
        }

        $result .= 'Täispindala leidmine.<br>';
        $result .= 'St = 6 * a<sup>2</sup><br>';
        $result .= 'St = '.$this->pindalaT($a).'<sup>2</sup><br><br>';
        $st = $this->pindalaT($a);

        $result .= 'Ruumala leidmine.<br>';
        $result .= 'V = a<sup>3</sup><br>';
        $result .= 'V = '.$this->ruumala($a).'<sup>3</sup><br><br>';
        $v = $this->ruumala($a);

        return $this->output($a, $st, $v, $result);
    }

    function umbermoot($a) {
        return $a * 4;
    }

    function pindalaT($a) {
        $st = 'undefined';

        if(is_numeric($a)) {
            $st = 6 * pow($a, 2);
        }

        return $st;
    }

    function ruumala($a) {
        $v = 'undefined';

        if(is_numeric($a)) {
            $v = pow($a, 3);
        }

        return $v;
    }

    function output($a, $st, $v, $result)  {
        is_numeric($a) ? $data['a'] = $a : $data['a'] = '-';
        is_numeric($st) ? $data['st'] = $st : $data['st'] = '-';
        is_numeric($v) ? $data['v'] = $v : $data['v'] = '-';
        $data['result'] = $result;
        return $data;
    }
}