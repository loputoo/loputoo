<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/18/2018
 * Time: 6:21 PM
 */

namespace App\Classes;


class calc_rectangle
{
    public function instantiate($a, $b, $d1) {
        $result = '';

        if(!is_numeric($a) && !is_numeric($b) && !is_numeric($d1)) {
            return array('error' => 'Ristküliku lahendamiseks on vaja kas kahte külge või diagonaal ja üks külgedest.');
        }

        if(!is_numeric($d1) && (is_numeric($a) && is_numeric($b))) {
            $result .= 'Diagonaali leidmine(Pythagorase teoreem).<br>';
            $result .= 'd<sub>1</sub> = &Sqrt;(a<sup>2</sup> + b<sup>2</sup>)<br>';
            $result .= 'd<sub>1</sub> = '.number_format(sqrt(pow($a, 2) + pow($b, 2)), 2).'<br><br>';
            $d1 = number_format(sqrt(pow($a, 2) + pow($b, 2)), 2);
        }

        if(!is_numeric($a) && (is_numeric($d1) && is_numeric($b))) {
            $result .= 'Külje a leidmine(Pythagorase teoreem).<br>';
            $result .= 'a = &Sqrt;(d<sub>1</sub><sup>2</sup> - b<sup>2</sup>)<br>';
            $result .= 'a = '.number_format(sqrt(pow($d1, 2) - pow($b, 2)), 2).'<br><br>';
            $a = number_format(sqrt(pow($d1, 2) - pow($b, 2)), 2);
        }

        if(!is_numeric($b) && (is_numeric($d1) && is_numeric($a))) {
            $result .= 'Külje b leidmine(Pythagorase teoreem).<br>';
            $result .= 'b = &Sqrt;(d<sub>1</sub><sup>2</sup> - a<sup>2</sup>)<br>';
            $result .= 'b = '.number_format(sqrt(pow($d1, 2) + pow($a, 2)), 2).'<br><br>';
            $b = number_format(sqrt(pow($d1, 2) - pow($a, 2)), 2);
        }

        $result .= 'Ümbermõõdu leidmine.<br>';
        $result .= 'P = 2 * (a + b)<br>';
        $result .= 'P = '.$this->umbermoot($a, $b).'<br><br>';
        $p = $this->umbermoot($a, $b);

        $result .= 'Pindala leidmine.<br>';
        $result .= 'S = a * b<br>';
        $result .= 'S = '.$this->pindala($a, $b).'<br><br>';
        $s = $this->pindala($a, $b);

        return $this->output($a, $b, $d1, $p, $s, $result);
    }

    public function umbermoot($a, $b) {
        return 2 * ($a + $b);
    }

    public function pindala($a, $b) {
        return $a*$b;
    }

    public function output($a, $b, $d1, $p, $s, $result)  {
        is_numeric($a) ? $data['a'] = $a : $data['a'] = '-';
        is_numeric($b) ? $data['b'] = $b : $data['b'] = '-';
        is_numeric($d1) ? $data['d1'] = $d1 : $data['d1'] = '-';
        is_numeric($p) ? $data['p'] = $p : $data['p'] = '-';
        is_numeric($s) ? $data['s'] = $s : $data['s'] = '-';
        $data['result'] = $result;
        return $data;
    }
}