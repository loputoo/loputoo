<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/18/2018
 * Time: 6:21 PM
 */

namespace App\Classes;


class calc_square
{
    public function instantiate($a) {
        $result = '';

        if(!is_numeric($a)) {
            return array('error' => 'Ruudu lahendamiseks sisestage külje pikkus!');
        }

        $result .= 'Ümbermõõdu leidmine.<br>';
        $result .= 'P = 4a = a + a + a + a<br>';
        $result .= 'P = '.$this->umbermoot($a).'<br><br>';
        $p = $this->umbermoot($a);

        $result .= 'Pindala leidmine.<br>';
        $result .= 'S = a<sup>2</sup><br>';
        $result .= 'S = '.$this->pindala($a).'<br><br>';
        $s = $this->pindala($a);

        return $this->output($a, $p, $s, $result);
    }

    function umbermoot($a) {
        return $a * 4;
    }

    function pindala($a) {
        return pow($a, 2);
    }

    function output($a, $p, $s, $result)  {
        is_numeric($a) ? $data['a'] = $a : $data['a'] = '-';
        is_numeric($p) ? $data['p'] = $p : $data['p'] = '-';
        is_numeric($s) ? $data['s'] = $s : $data['s'] = '-';
        $data['result'] = $result;
        return $data;
    }
}