<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/25/2018
 * Time: 2:17 PM
 */

namespace App\Classes;


class calc_paralleloPrism {
    public function instantiate($a, $b, $h) {
        $result = '';

        if(!is_numeric($a) && !is_numeric($b) && !is_numeric($h)) {
            return array('error' => 'Rööptahuka lahendamiseks täitke kõik väljad!');
        }

        $result .= 'Ümbermõõdu leidmine.<br>';
        $result .= 'P = 2*(a+b)<br>';
        $result .= 'P = '.$this->umbermoot($a, $b).'<sup>2</sup><br><br>';
        $p = $this->umbermoot($a, $b);
        if(!is_numeric($p)) {
            return array('error' => 'Rööptahuka ümbermõõdu leidmiseks sisestage kõik küljed!');
        }

        $result .= 'Põhja pindala leidmine.<br>';
        $result .= 'S<sub>p</sub> = a * b<br>';
        $result .= 'S<sub>p</sub> = '.$this->pindalaP($a, $b).'<sup>2</sup><br><br>';
        $sp = $this->pindalaP($a, $b);
        if(!is_numeric($sp)) {
            return array('error' => 'Rööptahuka põhja pindala lahendamiseks sisestage kõik küljed!');
        }

        $result .= 'Külgpindala leidmine.<br>';
        $result .= 'S<sub>k</sub> = P * H<br>';
        $result .= 'S<sub>k</sub> = '.$this->pindalaK($p, $h).'<sup>2</sup><br><br>';
        $sk = $this->pindalaK($p, $h);
        if(!is_numeric($sk)) {
            return array('error' => 'Rööptahuka külgpindala lahendamiseks sisestage kõik küljed!');
        }

        $result .= 'Täispindala leidmine.<br>';
        $result .= 'S<sub>t</sub> = 2 * S<sub>p</sub> + S<sub>k</sub><br>';
        $result .= 'S<sub>t</sub> = '.$this->pindalaT($sp, $sk).'<sup>2</sup><br><br>';
        $st = $this->pindalaT($sp, $sk);
        if(!is_numeric($st)) {
            return array('error' => 'Rööptahuka täispindala lahendamiseks sisestage kõik küljed!');
        }

        $result .= 'Ruumala leidmine.<br>';
        $result .= 'V = S<sub>p</sub> * H<br>';
        $result .= 'V = '.$this->ruumala($sp, $h).'<sup>3</sup><br><br>';
        $v = $this->ruumala($sp, $h);
        if(!is_numeric($v)) {
            return array('error' => 'Rööptahuka ruumala lahendamiseks sisestage kõik küljed!');
        }

        return $this->output($a, $b, $h, $p, $sp, $sk, $st, $v, $result);
    }

    function umbermoot($a, $b) {
        $sk = 'undefined';

        if(is_numeric($a) && is_numeric($b)) {
            $sk = 2 * ($a + $b);
        }

        return $sk;
    }

    function pindalaP($a, $b) {
        $sk = 'undefined';

        if(is_numeric($a) && is_numeric($b)) {
            $sk = $a * $b;
        }

        return $sk;
    }

    function pindalaK($p, $h) {
        $sk = 'undefined';

        if(is_numeric($p) && is_numeric($h)) {
            $sk = $p * $h;
        }

        return $sk;
    }

    function pindalaT($sp, $sk) {
        $st = 'undefined';

        if(is_numeric($sp) && is_numeric($sk)) {
            $st = 2 * $sp + $sk;
        }

        return $st;
    }

    function ruumala($sp, $h) {
        $v = 'undefined';

        if(is_numeric($sp) && is_numeric($h)) {
            $v = $sp * $h;
        }

        return $v;
    }

    function output($a, $b, $h, $p, $sp, $sk, $st, $v, $result)  {
        is_numeric($a) ? $data['a'] = $a : $data['a'] = '-';
        is_numeric($b) ? $data['b'] = $b : $data['b'] = '-';
        is_numeric($p) ? $data['p'] = $p : $data['p'] = '-';
        is_numeric($h) ? $data['h'] = $h : $data['h'] = '-';
        is_numeric($sp) ? $data['sp'] = $sp : $data['sp'] = '-';
        is_numeric($sk) ? $data['sk'] = $sk : $data['sk'] = '-';
        is_numeric($st) ? $data['st'] = $st : $data['st'] = '-';
        is_numeric($v) ? $data['v'] = $v : $data['v'] = '-';
        $data['result'] = $result;
        return $data;
    }
}