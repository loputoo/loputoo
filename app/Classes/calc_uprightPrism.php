<?php
/**
 * Created by PhpStorm.
 * User: Aix
 * Date: 5/25/2018
 * Time: 1:27 PM
 */

namespace App\Classes;


class calc_uprightPrism{
    public function instantiate($p, $sp, $h) {
        $result = '';

        if(!is_numeric($p) && !is_numeric($sp) && !is_numeric($h)) {
            return array('error' => 'Püstprisma lahendamiseks täitke kõik väljad!');
        }

        $result .= 'Külgpindala leidmine.<br>';
        $result .= 'Sk = P * h<br>';
        $result .= 'Sk = '.$this->pindalaK($p, $h).'<sup>2</sup><br><br>';
        $sk = $this->pindalaK($p, $h);
        if(!is_numeric($sk)) {
            return array('error' => 'Püstprisma külgpindala lahendamiseks täitke kõik väljad!');
        }

        $result .= 'Täispindala leidmine.<br>';
        $result .= 'St = 2 * Sp + Sk<br>';
        $result .= 'St = '.$this->pindalaT($sp, $sk).'<sup>2</sup><br><br>';
        $st = $this->pindalaT($sp, $sk);
        if(!is_numeric($st)) {
            return array('error' => 'Püstprisma täispindala lahendamiseks täitke kõik väljad!');
        }

        $result .= 'Ruumala leidmine.<br>';
        $result .= 'V = Sp * h<br>';
        $result .= 'V = '.$this->ruumala($sp, $h).'<sup>3</sup><br><br>';
        $v = $this->ruumala($sp, $h);
        if(!is_numeric($v)) {
            return array('error' => 'Püstprisma ruumala lahendamiseks täitke kõik väljad!');
        }

        return $this->output($p, $h, $sp, $sk, $st, $v, $result);
    }

    function pindalaK($p, $h) {
        $sk = 'undefined';

        if(is_numeric($p) && is_numeric($h)) {
            $sk = $p * $h;
        }

        return $sk;
    }

    function pindalaT($sp, $sk) {
        $st = 'undefined';

        if(is_numeric($sp) && is_numeric($sk)) {
            $st = 2 * $sp + $sk;
        }

        return $st;
    }

    function ruumala($sp, $h) {
        $v = 'undefined';

        if(is_numeric($sp) && is_numeric($h)) {
            $v = $sp * $h;
        }

        return $v;
    }

    function output($p, $h, $sp, $sk, $st, $v, $result)  {
        is_numeric($p) ? $data['p'] = $p : $data['p'] = '-';
        is_numeric($h) ? $data['h'] = $h : $data['h'] = '-';
        is_numeric($sp) ? $data['sp'] = $sp : $data['sp'] = '-';
        is_numeric($sk) ? $data['sk'] = $sk : $data['sk'] = '-';
        is_numeric($st) ? $data['st'] = $st : $data['st'] = '-';
        is_numeric($v) ? $data['v'] = $v : $data['v'] = '-';
        $data['result'] = $result;
        return $data;
    }
}