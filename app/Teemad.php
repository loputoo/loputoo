<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teemad extends Model
{
    //
    protected $fillable = ['name'];
}
