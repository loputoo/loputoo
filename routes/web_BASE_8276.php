<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PageController@index');
Route::get('{catid}/teemad', ['uses' =>'PageController@viewThemes']);
Route::get('teema/{id}', ['uses' => 'PageController@openTheme']);
Route::get('teema/{id}/{page}', ['uses' => 'PageController@openTheme']);
