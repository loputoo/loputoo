<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PageController@index');
Route::get('{catid}/teemad', ['uses' =>'PageController@viewThemes']);
Route::get('teema/{id}', ['uses' => 'PageController@openTheme']);
<<<<<<< HEAD
Route::post('teema/{id}/calc/triangle/', ['uses' => 'PageController@triangleCalc']);
=======
Route::get('teema/{id}/{page}', ['uses' => 'PageController@openTheme']);

Auth::routes();

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::resource('tests', 'TestsController');
});

Route::delete('tests/mass_destroy', 'TestsController@massDestroy')->name('tests.mass_destroy');
Route::resource('tests', 'TestsController');

Route::get('/home', 'HomeController@index')->name('home');
>>>>>>> origin/rauno
