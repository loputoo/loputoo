<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PageController@index');
Route::get('{catid}/teemad', ['uses' =>'PageController@viewThemes']);
Route::get('teema/{id}', ['uses' => 'PageController@openTheme']);
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::resource('tests', 'TestsController');
    Route::resource('users', 'UserController');
    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');
});
Route::delete('tests/mass_destroy', 'TestsController@massDestroy')->name('tests.mass_destroy');
Route::get('/home', 'HomeController@index')->name('home');

//Routes for tests
Route::post('teema/{id}/getTests', ['uses' => 'PageController@getTests']);
Route::post('teema/{id}/getResults', ['uses' => 'PageController@getResults']);

//Routes for calculators
Route::post('teema/{id}/calc/triangle/', ['uses' => 'PageController@triangleCalc']);
Route::post('teema/{id}/calc/square/', ['uses' => 'PageController@squareCalc']);
Route::post('teema/{id}/calc/rectangle/', ['uses' => 'PageController@rectangleCalc']);
Route::post('teema/{id}/calc/rhombus/', ['uses' => 'PageController@rhombusCalc']);
Route::post('teema/{id}/calc/parallelogram/', ['uses' => 'PageController@parallelogramCalc']);
Route::post('teema/{id}/calc/trapesium/', ['uses' => 'PageController@trapesiumCalc']);
Route::post('teema/{id}/calc/cube/', ['uses' => 'PageController@cubeCalc']);
Route::post('teema/{id}/calc/rectangular/', ['uses' => 'PageController@rectangularCalc']);
Route::post('teema/{id}/calc/uprightPrism/', ['uses' => 'PageController@uprightPrismCalc']);
Route::post('teema/{id}/calc/paralleloPrism/', ['uses' => 'PageController@paralleloPrismCalc']);


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');



// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
